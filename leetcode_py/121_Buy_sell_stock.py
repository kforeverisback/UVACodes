import pprint
from selectors import EpollSelector

class Solution:
    def maxProfit(self, prices: list[int]) -> int:
        buy = prices[0]
        max_profit = 0
        for i in range(1, len(prices)):
            if prices[i] > buy:
                profit = prices[i] - buy
                max_profit = max(max_profit, profit)
            else:
                buy = prices[i]
        return max_profit

"""
Input: prices = [7,1,5,3,6,4]
Output: 5

Input: prices = [7,6,4,3,1]
Output: 0
7 , 1 , 5 , 3 , 6 , 4
5 b=5
2 b=2,mp=0
1 b=1,mp=0
5 b=1,p=4,mp=4
7 b=1,
9

1,2,3,4,5,6,7
1,7,6,5,4,3,2,10
"""
if __name__ == "__main__":
    test_cases = [
        [5,2,1,5,7,9],
        [5,1,1,1,1,9],
        [2,1,1,1,1,6],
        [1],
        [2,1],
        [1,2],
        [7,1,5,3,6,4],
        [1,2,3,4,5],
        [7,6,4,3,1],
        [1,2,3,4,5,5,4,3,2,1],
        [6,5,4,3,1,2,3,4,5,7],
        [7,1,4,3,1,1,1,4,5,7]
    ]
    for tc in test_cases:
        print(f"Test Case: {tc}: {Solution().maxProfit(tc)}")
