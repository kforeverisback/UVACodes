from pprint import pprint
class Solution:
    def twoSum(self, nums: list[int], target: int):
        required_dict = {}
        for i,v in enumerate(nums):
            if v in required_dict:
                return [required_dict[v], i]
            required_dict[target - v] = i
        return None

if __name__ == "__main__":
    print(Solution().twoSum([11,2,0,3,-5], -5))
    print(Solution().twoSum([11,2,0,8,0,7,3,5], 0))
    print(Solution().twoSum([11,-2,15,8,12,7,3,5], 9))
    print(Solution().twoSum([2, 7, 11, 15], 9))
    print(Solution().twoSum([3,2,4], 6))
    print(Solution().twoSum([3,3,8], 6))
    print(Solution().twoSum([3,23,0,11,5,88], 23))
