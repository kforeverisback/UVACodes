# Leetcode Solutions

The following list of problems are listed in [this link](https://github.com/Pankaj-Kharkwal/DSA/blob/master/Notes.md).

|  #  | Level | Description      | Comments  |
| --- | ----- | ---------------- | --------- |
|  1  | Easy  | [Two Sum](https://leetcode.com/problems/two-sum/) ||
| 121 | Easy  | [Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/)| Had to look up my old solution. It fails to mention its looking for one buy and sell (where you get the max) not multiple ones summed up together |
| 26  | Easy   | [Remove Duplicates from Sorted Array](https://leetcode.com/problems/remove-duplicates-from-sorted-array/) | |
