import pprint
import random

class Solution:
    def removeDuplicates(self, nums: list[int]) -> int:
        cp = 0
        unique = 1 # since we must have at least one number
        for i in range(1, len(nums)):
            if nums[i] != nums[i-1]:
                cp+=1
                unique+=1
                nums[cp] = nums[i]
        return unique

"""
Input: nums = [1,1,2]
Output: 2, nums = [1,2,_]
Explanation: Your function should return k = 2, with the first two elements of nums being 1 and 2 respectively.
It does not matter what you leave beyond the returned k (hence they are underscores).
rc = 0
0    c=0             saved
0 == c=0 rc=1
0 == c=0 rc=2
1 != c++,c=1, c[1] = this
1 
1
2
3
4
5
Input: nums = [0,0,1,1,1,2,2,3,3,4]
0      c=0
0 0==0 c=0           1
1 1!=0 c=1, c[1] = 1 2
1 1==1 c=1
1 1==1 c=1
2 2!=1 c=2, c[2] = 2 
2 2==2 c=2
3 3!=2 c=3  c[3] = 3
3 3==3 c=3
4 4!=3 c=4, c[4] = 4
Output: 5, nums = [0,1,2,3,4,_,_,_,_,_]
Explanation: Your function should return k = 5, with the first five elements of nums being 0, 1, 2, 3, and 4 respectively.
It does not matter what you leave beyond the returned k (hence they are underscores).
"""

def gen_test_cases(num_arrays, values, array_size):
    arrays = []
    for _ in range(num_arrays):
        array = [random.choice(values) for _ in range(array_size)]
        arrays.append(array)
    arrays.sort()
    return arrays

if __name__ == "__main__":
    test_cases = [
        [1,1,2],
        [0,0,1,1,1,2,2,3,3,4],
        [1,1,1,1,1,1,1,1,1,2],
        [1,1,1,1,1,1,1,1,1],
        [1,2,3,4],
        [1],
        [ 1, 5, 10, 10, 10, 10, 12, 14, 15, 25, 25, 30, 75, 92],
        [ 0,0,0,0, 2,9, 11, 11,11,15, 21, 27, 27, 41, 43, 43, 51, 61, 74, 81, 99,99,99,99],
        [ 8, 11,11,11, 11, 17, 21, 21, 22, 25, 29, 52, 57, 73, 98],
    ]
    # values = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    # test_cases.append(gen_test_cases(5, values, 10))
    for tc in test_cases:
        old_tc = tc.copy()
        num_sol = Solution().removeDuplicates(tc)
        print(f"Test Case: {old_tc}: {num_sol}, after: {[tc[i] for i in range(0,num_sol)]}")
    