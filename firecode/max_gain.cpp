#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
using namespace  std;
// Elegant solution
// typedef struct _g {
//     int min;
//     int max;
// } gain;
// int max_gain(int arr[], int sz)
// {
//     if( sz <= 1) return 0;
//     return std::accumulate(arr, arr + sz, gain{std::numeric_limits<int>::max(), 0},
//     [](gain g, int v) {
//         if(g.min > v) g.min = v;
//         else if (g.max < (v - g.min)) g.max = v - g.min;
//         return g;
//     }).max;
// }

//  This problem is interesting and can be solved in O(n)
// Apparently this solution is a Dynamic Programming Solution
int max_gain(int arr[], int sz)
{
    if(sz <= 1) return 0;

    int max_gain = 0,
        cur_min = INT32_MAX; // Meaning anything will be lower than INT32_MAX
    /**
     * We go through all values once
     * Calculate the max_gain by subtracting current value and cur minimum
     * The clue is to select the current minimum
     * Since, for difference large numbers will come after lower numbers
     * We don't have to save the minimum number possible for current value
     * By selecting cur_min = INT32_MAX, we're making sure the 
     * first number is selected. Then we go through the array,
     * either the next number will be lower, in which case we have to save
     * the new lower number. Then at some point if there is a higher number
     * we calculate the diff (our max gain).
     * For cases where there is even a lower number, we select the lower number
     * in the middle, and if there is a higher number we calculate diff
     * and make sure the max_gain is greater than the new one
     * with : max_gain = std::max(max_gain, arr[i] - cur_min);
    */
    for (int i = 0; i < sz; i++)
    {
        if (arr[i] < cur_min )
        {
            cur_min = arr[i];
        }
        else 
        {
            max_gain = std::max(max_gain, arr[i] - cur_min);
        }
    }

    return max_gain;
}

int main()
{
    vector<int> v2 = {0,50,10,100,-10,20,50, -5, 100};
    cout <<  max_gain(v2.data(), v2.size()) << endl;
}
