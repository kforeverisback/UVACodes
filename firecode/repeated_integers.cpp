/*
You're given a game board that has m x n squares on it, represented by an m x n array. Write a method - count_paths that takes in m and n and returns the number of possible paths from the top left corner to the bottom right corner. Only down and right directions of movement are permitted.

Note:
Your method should output the result in a reasonable amount of time for large values of m and n. If you're thinking of using DFS, consider the tree depth and branching factor for m and n > 15!
*/
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;
#define XX  ?"True":"False"
// Add any helper functions(if needed) above.
int* remove_dup(int A[], int sz)
{    /* Alocate the memory of Output array of maximum size n. 
    Fill the new array with unique elements, which have duplicates entries in input array 
    Size of Output array will be less than n. */
    // Add your code below this line. Do not modify any other code.
    std::sort(A, A + sz);
    int * curdup = nullptr;
    vector<int> temp;
    for(int i = 0;i < sz - 1; i++)
    {
        if(A[i] == A[i+1] && curdup == nullptr)
        {
            curdup = &A[i];
        }
        else if (curdup != nullptr)
        {
            temp.push_back(*curdup);
            curdup = nullptr;
        }
    }

    if(curdup != nullptr)
        temp.push_back(*curdup);

    // Add your code above this line. Do not modify any other code.
    int* output = new int[temp.size()];
    memcpy(output, temp.data(), temp.size() * sizeof(int));
    return output;
}

// Better solution using unordered_set
// #include <unordered_set>
// #include <set>
// // O( n +  klogk) : Where k is the number of duplicates
// int* remove_dup(int a[], int sz) {
//     if (sz == 1) return nullptr;
    
//     unordered_set<int> nums;
//     set<int> dups;
    
//     // O(n)
//     for (int i = 0; i < sz; i++) {
//         // O(klogk)
//         if (nums.find(a[i]) == nums.end())
//             nums.insert(a[i]);
//         else
//             dups.insert(a[i]);
//     }
    
//     int* output = new int[dups.size()];
//     int i = 0;
//     // O(n)
//     for (const int& dup : dups)
//         output[i++] = dup;
    
//     return output;
// }

int main()
{
    vector<int> v2 = {1,2,4,4,5,6,2,1,6};
    int * v = remove_dup(v2.data(), v2.size());
    while(v != nullptr)
    {
        printf("%d ", *v++);
    }
    return 0;
}