#include <iostream>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
using namespace std;

// Add any helper functions(if needed) below.
struct treeNode{
    int value;              /* value of the node */
    struct treeNode* left = nullptr;  /* pointer to the left child node */
    struct treeNode* right = nullptr; /* pointer to the right child node */
};

// Add any helper functions(if needed) above.
vector<int> level_order(treeNode* root)
{
    vector<int> preordered_list;
    if (root == nullptr) return preordered_list;
    // Basically doing a BFS
    queue<treeNode*> qn;
    qn.push(root);
    while (qn.size() != 0)
    {
        auto n = qn.front();
        qn.pop();
        preordered_list.push_back(n->value);
        if (n->left != nullptr)
            qn.push(n->left);
        if (n->right != nullptr)
            qn.push(n->right);
    }

    return preordered_list;
}

int main()
{
    int sz = 20;
    treeNode tall[sz];
    for(int i = 1; i < sz; i++)
    {
        tall[i].value = i;
    }
    tall[1].left = &tall[2];
    tall[1].right = &tall[3];


    tall[2].left = &tall[4];
    tall[2].right = &tall[5];


    tall[3].left = &tall[6];
    tall[3].right = &tall[7];

    tall[5].right = &tall[8];
    tall[7].right = &tall[9];
    auto v = level_order(&tall[1]);
    for (auto i:v){
        cout << i << " ";
    }
    return 0;
}
