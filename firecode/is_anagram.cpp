#include <iostream>
#include <string>
#include <cmath>
using namespace std;
#define XX ? "True":"False"
// Add any helper functions(if needed) above.
bool is_anagram(string input1, string input2)
{
    //cout << input1 << ","<<input2 << endl;
    if(input1.size() != input2.size())
        return false;

    int cnt[30]={0};
    for(int i = 0; i<input1.size(); i++)
    {
        cnt[input1[i] - 'a']++;
        cnt[input2[i] - 'a']--;
    }

    for (auto i:cnt)
    {
        if (i != 0) return false;
    }

    return true;
}

int main()
{
    cout << (is_anagram("yellow","llowey") XX) << endl;
    cout << (is_anagram("hello","hi") XX) << endl;
    cout << (is_anagram("abcde","acdbe") XX) << endl;
    cout << (is_anagram("h","h") XX) << endl;
    cout << (is_anagram("bc","cb") XX) << endl;
    cout << (is_anagram("db","db") XX) << endl;

    return 0;
}