#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
using namespace std;
#define XX ? "True":"False"
class treeNode{
    public:
        int value;              /* value of the node */
        treeNode* left = nullptr;  /* pointer to the left child node */
        treeNode* right = nullptr; /* pointer to the right child node */
};
#include <limits>
bool check_child(treeNode* root, int min, int max)
{
    if (!root)
        return true;

    if( root->value < min || root->value > max ) return false;
    if ( !check_child(root->left, min, root->value) ) return false;
    return check_child(root->right, root->value, max);
}

bool validate_bst(treeNode* root)
{
    return check_child(root, std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
}
int main()
{
    treeNode t[8];
    t[0].value = 20;
    t[1].value = 15;
    t[2].value = 40;
    t[3].value = 10;
    t[4].value = 30;
    t[5].value = 17;
    t[6].value = 40;

    t[7].value = 16;
    t[8].value = 16;

    t[0].left = &t[1];
    t[0].right = &t[2];

    t[1].left = &t[3];
    t[1].right = &t[4];

    t[2].left = nullptr;
    t[2].right = nullptr;

    cout << (validate_bst(&t[0]) XX) << endl;
    return 0;
}