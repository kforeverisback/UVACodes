#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
using namespace std;
#define XX ? "True":"False"
class treeNode{
    public:
        int value;              /* value of the node */
        treeNode* left = nullptr;  /* pointer to the left child node */
        treeNode* right = nullptr; /* pointer to the right child node */
    
    treeNode(){}
    treeNode(int n): value(n){}
    treeNode* operator=(int n)
    {
        this->value = n;
        return this;
    }
};

#include <queue>
typedef struct _tuple
{
    int node_max, node_min;
    treeNode* cur_node = nullptr;
} tpl;
bool validate_bst_itr(treeNode* root)
{
    if(!root) return true;
    std::queue<tpl> qn;
    tpl r;
    r.node_max = INT32_MAX;
    r.node_min = INT32_MIN;
    r.cur_node = root;
    qn.push(r);
    while(!qn.empty())
    {
        auto n = qn.front(); qn.pop();
        // Check if our current node corresponds to it's max and min
        if ( n.cur_node->value < n.node_min || n.cur_node->value > n.node_max )
            return false;
        // Then push left
        if (n.cur_node->left) {
            tpl temp;
            temp.cur_node = n.cur_node->left;
            temp.node_max = n.cur_node->value;
            temp.node_min = n.node_min;
            qn.push(temp);
        }
        // And push right
        if (n.cur_node->right) {
            tpl temp;
            temp.cur_node = n.cur_node->right;
            temp.node_max = n.node_max;
            temp.node_min = n.cur_node->value;
            qn.push(temp);
        }
    }
    return true;
}
#include <map>
int main()
{
    #define NULL_NODE INT32_MIN
    // INT32_MIN means NULL node
    vector<vector<int>> nodes = {
        {20,15,18},
        {15,10,16},
        {18,17,40},
        };
    map<int, treeNode> ts;
    for(auto i : nodes)
    {
        if ( i[0] != NULL_NODE && ts.find(i[0]) == ts.end() )
            ts[i[0]] = i[0];
        if ( i[1] != NULL_NODE )
        {
            if(ts.find(i[1]) == ts.end()) ts[i[1]] =i[1];
            ts[i[0]].left  = &ts[i[1]];
        }
        if ( i[2] != NULL_NODE )
        {
            if(ts.find(i[2]) == ts.end()) ts[i[2]] =i[2];
            ts[i[0]].right = &ts[i[2]];
        }
    }
    // ts[20].left = &ts[15];
    // ts[20].right = &ts[40];

    // ts[15].left = &ts[14];
    // ts[15].right = &ts[18];
    //treeNode nn = {11};
    cout << (validate_bst_itr(&ts[20]) XX) << endl;
    return 0;
}