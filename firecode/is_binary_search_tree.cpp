#include <iostream>
#include <numeric>
#include <vector>
#include <limits>
#include <functional>
using namespace std;
#define XX ? "True":"False"
// Add any helper functions(if needed) below.
struct treeNode{
    int value;              /* value of the node */
    struct treeNode* left = nullptr;  /* pointer to the left child node */
    struct treeNode* right = nullptr; /* pointer to the right child node */
};

// Add any helper functions(if needed) above.
//bool check_child(treeNode* root, int max, int min, std::function<bool(int,int)> cmp )
bool check_child(treeNode* root, int min, int max)
{
    if (!root)
        return true;

    if( root->value < min || root->value > max ) return false;
    if ( !check_child(root->left, min, root->value) ) return false;
    return check_child(root->right, root->value, max);
}

bool validate_bst(treeNode* root)
{
    if(!root) return true;

    if ( !check_child(root->left, std::numeric_limits<int>::min(), root->value)) return false;
    return check_child(root->right, root->value, std::numeric_limits<int>::max());

}

#define LRN(_ptr_) (_ptr_)->left = (_ptr_)->right=nullptr
int main()
{
    treeNode tall[10];
    tall[0].left = &tall[1];
    tall[0].right = &tall[2];
    tall[0].value = 20;

    tall[1].left = &tall[3];
    tall[1].right = &tall[4];
    tall[1].value = 15;

    tall[2].left = &tall[5];
    tall[2].right = &tall[6];
    tall[2].value = 18;

    tall[3].value = 10;
    tall[4].value = 16;
    tall[5].value = 17;
    tall[6].value = 40;
    //tall[5].value = 17;

    LRN(&tall[3]);
    LRN(&tall[4]);
    LRN(&tall[5]);
    LRN(&tall[6]);
    cout << ( validate_bst(&tall[0]) XX ) <<endl;
    return 0;
}
