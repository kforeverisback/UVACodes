#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
using namespace std;
#define XX ? "True":"False"
struct treeNode
{
    int value = -1;              /* value of the node */
    struct treeNode* left = nullptr;  /* pointer to the left child node */
    struct treeNode* right = nullptr; /* pointer to the right child node */
};

treeNode* find_lca_pair(treeNode* root, vector<int>& ab)
{
    // If all values are check or root is null then we're done
    if(!root || ab.size() == 0) return nullptr;
    
    // Whether we found it right at this node
    bool this_found = false;
    auto it = std::find(ab.begin(), ab.end(), root->value);
    // If matches, then remove it from ab vector
    if (it != ab.end())
    {
        ab.erase(it);
        this_found = true;
    }

    auto rptr = find_lca_pair(root->right, ab);
    auto lptr = find_lca_pair(root->left, ab);
    // This single line is very important
    // If this node has one value, and it's children have another, then this node is the lca node
    // Or if this node doesn't have target values, then check whether both their child have it
    this_found |= (lptr != nullptr && rptr != nullptr);
    if (this_found)
    {
        // If any of the above is true, return this node
        return root;
    }
    
    // Otherwise, return left or right depending on which one is not null
    return (lptr?lptr:rptr);

    return nullptr;
}
treeNode* find_lca(treeNode* root, int a, int b)
{
    vector<int> ab; ab.push_back(a);ab.push_back(b);
    return find_lca_pair(root, ab);
}
/** Actual Solution **/
// treeNode* find_lca(treeNode* root, int a, int b) 
// {
//     /* reach the end of the tree . return 0 */
//     if (root == NULL) return NULL;
//     /*Initialising temporary left and right pointers */
//     treeNode *left=NULL, *right = NULL;
//     /* If any of the child node has one of the input values, root is the ancestor */
//     if (root->value == a || root->value == b) return root;
//     left = find_lca(root->left, a, b);      /* Find LCA in left subtree */  
//     right = find_lca(root->right, a, b);    /* Find LCA in right subtree */

//     /* If nodes found in both the subtrees, root is there Least common ancestor */
//     if (left != NULL && right != NULL) {
//         return root;
//     } else /* If one of the subtree has values (a or b or both), return the node */
//         return (left != NULL ? left : right);
// }

int main()
{
    treeNode t[16]={0};
    t[0].value = 1;
    t[1].value = 2;
    t[2].value = 3;
    t[3].value = 4;
    t[4].value = 5;
    t[5].value = 6;
    t[6].value = 7;
    t[7].value = 8;
    t[8].value = 32;

    t[0].left = &t[1];
    t[0].right = &t[2];

    t[1].left = &t[3];
    t[1].right = &t[4];

    t[2].right = &t[5];
    t[2].left = &t[6];

    t[5].right = &t[7];
    t[5].left = &t[8];
 
    // t[4].left = &t[6];
    // t[4].right = &t[7];

    // t[5].left = &t[8];
    // t[5].right = nullptr;
   

    cout << find_lca(&t[0],3,32)->value << endl;
    return 0;
}

/**
 * Elegant Solution!!!!!
pair<int, int> dia(treeNode* root) {
    if(!root) return {0, 0};
    
    auto l = dia(root->left);
    auto r = dia(root->right);
    return {
        max(l.first, r.first) + 1,
        max(l.first + r.first + 1, max(l.second, r.second))
    };
}
// Add any helper functions(if needed) above.
int diameter(treeNode* root)
{
    return dia(root).second;
}
*/