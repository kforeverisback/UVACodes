#include <iostream>
#include <string>
#include <cmath>
#include <cstring>
#include <vector>
#include <stack>
using namespace std;
#define XX ? "True":"False"
struct listNode{
    int value = -1;               /* value of the node */
    struct listNode* next = nullptr;   /* points to the next linked-list node */
};

// Add any helper functions(if needed) above.
listNode* find_nxt_n(listNode* head, int& n)
{
    std::stack<listNode*> nstack;
    listNode* nxt=head;
    while(nxt != nullptr)
    {
        nstack.push(nxt);
        nxt=nxt->next;
    }
    listNode* rnth = nullptr;
    while(nstack.size() > 0 && n >= 0)
    {
        listNode* p = nstack.top();nstack.pop();
        if (n == 0) rnth = p;
        else n--;
    }
    return rnth;
    
}
listNode* find_n_node_from_end(listNode* head, int n)
{
    std::stack<listNode*> nstack;
    listNode* nxt=head;
    while(nxt != nullptr)
    {
        nstack.push(nxt);
        nxt=nxt->next;
    }
    listNode* rnth = nullptr;
    while(nstack.size() > 0 && n >= 0)
    {
        listNode* p = nstack.top();nstack.pop();n--;
        if (n == 0) { rnth = p; break;}
    }
    return rnth;
}
/// Own
listNode NODES[100];
listNode* c(std::vector<int> arr)
{
    if(arr.size() == 0 ) return nullptr;
    std::memset(&NODES[0], 0, sizeof(listNode) * 100);
    listNode* n = &NODES[0];
    for(int i = 0; i < arr.size(); i++)
    {
        NODES[i].value = arr[i];
        NODES[i].next = &NODES[i+1];
    }
    NODES[arr.size() - 1].next = nullptr;

    return &NODES[0];
}
void p(listNode* h, int cnt = -1)
{
    if(h == nullptr)
        cout << "{null}";

    while(h!=nullptr)
    {
        if (cnt != 0)
        {
            cout << h->value << "->";
            h = h->next;
            cnt--;
        }
        else break;
    }
    cout << endl;
}

int main()
{
    listNode* r = c({});
    p(find_n_node_from_end(r, 6), 1);
    return 0;
}