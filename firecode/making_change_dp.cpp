#include <iostream>
#include <string>
#include <cmath>
#include <cstring>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>
using namespace std;
#define pv(_X_) copy(_X_.begin(), _X_.end(), ostream_iterator<int>(cout," "));cout << endl
#define pva(_X_, _sz_) copy((_X_), (_X_) + (_sz_), ostream_iterator<int>(cout," ")); cout << endl
// Add any helper functions(if needed) above.
/**
 * It's the famous Coin Change problem solved using DP
 * Coins: 1 2 3 4 5
 * Amont: 5
 * Table:
 *            | 0   1   2   3   4   5
 *         [] | 1   0   0   0   0   0
 *         *1 | 1   1   1   1   1   1
 *       1,*2 | 1   1   2   2   3   3
 *     1,2,*3 | 1   1   2   3   4   5
 *   1,2,3,*4 | 1   1   2   3   5   6
 * 1,2,3,4,*5 | 1   1   2   3   5   7*
 * 
 * Coins: 25 10 5 1  (sorted 1,5,10,25)
 * Amont: 10
 *            | 0 1 2 3 4 5 6 7 8 9 10
 *         [] | 1 0 0 0 0 0 0 0 0 0 0
 *         *1 | 1 1 1 1 1 1 1 1 1 1 1 
 *       1,*5 | 1 1 1 1 1 2 2 2 2 2 3 
 *    1,5,*10 | 1 1 1 1 1 2 2 2 2 2 4 
 * 1,5,10,*25 | 1 1 1 1 1 2 2 2 2 2 4* 
*/
int make_change(int coins[], int n, int amount)
{
    if(amount < 0) return 0;
    // Sorting is not necessary, but easy to work with at first
    //std::sort(coins, coins + n, std::less<int>());

    // int *prev = new int[amount + 1],
    //     *cur  = new int[amount + 1];
    // memset(prev, 0, sizeof(int) * (amount + 1));
    // Assuming max sum is 1024.
    int prev[1024]={0}, cur[1024]={0};
    // When there are no coins, then there is Zero ways to make changes
    // When there are coins, but we want to make change for amount Zero,
    // one way to make change for mount Zero. Which is not doing anything
    for(int i = 0; i < n; i++)
    {
        int cur_coin = coins[i];
        prev[0] = 1; // For the base case where Amount is zero and you have coins
        cur[0]  = 1;
        //pva(prev, amount + 1);
        for (int j = 1; j <= amount; j++)
        {
            cur[j] = prev[j];
            if(cur_coin <= j)
                cur[j] += cur[j - cur_coin];
        }
        //pva(cur, amount + 1);
        std::swap(cur, prev);
    }

    return prev[amount];
}

// // Firecode Solution
// int count_coins(int coins[],int n, int amount, int current_coin_index) 
// {
//     int next_coin_index;
//     if (amount == 0)      
//         return 1;
//     if (current_coin_index == -1)       /* Ran out of the denominations */
//         return 0;
//     if (current_coin_index < n-1)       /* Keep a track of the next coin */
//         next_coin_index = current_coin_index + 1;
//     else                                /* the current coin is last coin */
//        next_coin_index = -1;
//     int res = 0;
//      /* Try different number of coins which can possibly be used add upto the amount*/
//     for (int i = 0; i * coins[current_coin_index] <= amount; i++)
//         res += count_coins(coins,n, amount-i*coins[current_coin_index],next_coin_index);
//     return res;
// }
// // Add any helper functions(if needed) above.
// int make_change(int coins[], int n, int amount) 
// {
//     if (amount<=0 ) 
//          return 0;
//     if (n > 0)
//         return count_coins(coins,n,amount,0);
//     return 0;
// }

int main()
{
    // 1 1 1 1 1
    // 1 1 1 2
    // 1 1 3
    // 1 4
    // 3 2
    // 2 2 1
    // 5 
    vector<int> v = {25,1,10,5};
    //vector<int> v = {4,2,5,3,1};
    pv(v);cout << endl;
    cout << make_change(v.data(), v.size(), 10) << endl;
    return 0;
}