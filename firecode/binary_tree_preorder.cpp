#include <iostream>
#include <numeric>
#include <vector>
using namespace std;

vector<int> preordered_list;
// Add any helper functions(if needed) below.
struct treeNode{
    int value;              /* value of the node */
    struct treeNode* left = nullptr;  /* pointer to the left child node */
    struct treeNode* right = nullptr; /* pointer to the right child node */
};

// Add any helper functions(if needed) above.
void preorder(treeNode* root)
{
    if (root)
    {
        preordered_list.push_back(root->value);
        preorder(root->left);
        preorder(root->right);
    }
}

int main()
{
    treeNode tall[8];
    for(int i = 1; i <= 7; i++)
    {
        tall[i].value = i;
    }
    tall[1].left = &tall[2];
    tall[1].right = &tall[3];


    tall[2].left = &tall[4];
    tall[2].right = &tall[5];


    tall[3].left = &tall[6];
    tall[3].right = &tall[7];
    preorder(&tall[1]);
    for (auto i:preordered_list){
        cout << i << " ";
    }
    return 0;
}
