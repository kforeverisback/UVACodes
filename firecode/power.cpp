#include <iostream>
#include <numeric>
#include <cmath>
using namespace std;
/*
double power_plus(double x, int n)
{
    double res = 1;
    for (int i = 0; i < n; i++)
    {
        res *= x;
    }
    return res;
}
double power(double x, int n)
{
    if (x == 1) return 1;
    if (x == 0) return 0;
    int p = std::abs(n);
    auto result = power_plus(x, p);
    if(n < 0)
        return 1 / result;
    return result;
}*/

// Lesser calculations
double power_plus(double x, int n)
{

    //printf("x %.2f, n %d\n", x,n);
    if (n == 1 ) return x;
    if (x == 0) return 0;
    if (n == 0 || x == 1) return 1;
    double res = 1;
    if (n % 2 == 0)
    {
        // We're reducing the number of multiplications by half everytime
        // hence the complexity is O(log(n))
        res = power_plus(x, n/2);
        // Then we're multiplying two results
        return res * res;
    }
    // We're changing the power to even then we're multiplying one extra 'x' at the end
    res = power_plus(x, (n-1)/2);
    return res * res * x;
}
double power(double x, int n)
{
    if(n < 0)
        return 1 / power_plus(x, std::abs(n));
    return power_plus(x, n);
}

int main()
{
    cout << power(2, 3) << " " << power(4,-2) << " " << power(5, 2) << " " << power (2, -10);
    return 0;
}
