#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
// Some methods and signatures you'll find useful.
class Interval {
    public:
        int start;
        int end;
        Interval(){}
        Interval(int start, int end) {
            this->start = start;
            this->end = end;
        }
};
// Since we already have a sorted non-overlapping intervals list,
// And we have an initial interval (insert) to merge into
// Rather than merging intervals into the insert interval,
// We'll merge the insert interval into the intervals_list ;)
// Makes it O(n) and less processing
/*
0
current:       5----7
target : 1---3
or
current:      4----7
target : 1---3
1
current:     3----7
target : 1---3
2
current:   4----7
target : 1---5
3
current:  4-----7
target :   4--6
4
current:   4---7
target : 1-------9
5
current: 4----7
target :   6------9
6
current: 4----7
target :      7---9
7
current: 4----7
target :       8---10
or
current: 4----7
target :         9---10
*/
vector<Interval> insert_range(vector<Interval> intervals_list, Interval insert) 
{
    vector<Interval> output;
    Interval* target = &insert;
    // Either insert or merge
    for(int i = 0;i < intervals_list.size(); i++)
    {
        // If no target. the just the rest of intervals// Case: 0
        if(!target ) output.push_back(intervals_list[i]);
        // Target in the front
        else if( (target->end /* + 1*/) < intervals_list[i].start && target->start < intervals_list[i].start )
        {
            output.push_back(*target);
            target = nullptr;
            output.push_back(intervals_list[i]);
        }
        // Target in the middle // Case: 1~6
        else if (target->start <= (intervals_list[i].end /* + 1*/))
        {
            // Set the target Start// case: 1-3
            // Case 3: When the target is completely covered by an existing interval
            // The target changes to this interval here.
            if(target->start > intervals_list[i].start)
            {
                target->start = intervals_list[i].start;
            }
            // Set the target End //Case 5-6
            if(target->end < intervals_list[i].end)
            {
                target->end = intervals_list[i].end;
            }
            // Don't add this target here. Rather wait for it to get more merged or overlap other intervals
        }
        // Target in the back, handled after the for loop. Meaning we add this interval to output
        else{ // Case 7
            output.push_back(intervals_list[i]);
        }
    }

    // If the target is not already added then, add it in the back
    // Case 7
    if(target) output.push_back(*target);

    return output;
}

// vector<Interval> insert_range(vector<Interval> intervals_list, Interval insert) 
// {
//     vector<Interval> result;
//     Interval prev;
//     for (int i = 0; i < intervals_list.size(); i++)
//     {
//         prev = intervals_list.at(i);
//         if(prev.end < insert.start)
//             result.push_back(prev);
//         else if(prev.start > insert.end) 
//         {
//             result.push_back(insert);
//             insert = prev;
//         }
//         else if(prev.start <= insert.end || prev.end >= insert.start)
//         {
//             int new_start = min(prev.start, insert.start);
//             int new_end = max(prev.end, insert.end);
//             insert = Interval(new_start, new_end);
//         }
//     }
//     result.push_back(insert);
//     return result;
// }

int main()
{
    vector<vector<Interval>> v={
        {{1,2}, {3,5}, {6,7}, {8,10}, {12,14}, /*insert*/{5,9}},
        {{1,5},{7,10},/*Insert*/{2,4}},
        {{-10,-5},{0,10}, /*Insert*/{-2,-1}},
        {{},{1,2}},
        {{0,5},/*Insert*/{1,2}},
        {{1,4},{7,9},/*Insert*/{2,6}},
        {{0,1},{4,5},/*Insert*/{2,3}},
        {{0,1},{3,4},/*Insert*/{2,10}},
        };
    for(auto v1 : v)
    {
        auto insert = v1.back(); v1.pop_back();
        if(v1[0].end == v1[0].start) v1.pop_back();
        auto kk = insert_range(v1, insert);
        for (auto i : kk)
        {
            cout << i.toString() << " ";
        }
        cout << endl;
    }
    return 0;
}