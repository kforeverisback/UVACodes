// namespace std have been included for this problem.
#include <algorithm>
#include <iostream>
#include <numeric>
using namespace std;

// Add any helper functions(if needed) above.
int* merge(int arr_left[],int sz_left, int arr_right[], int sz_right){
    /* declaring an array using dynamic allocation of memory. 
        Merged the given two arrays into this third output Array */
    int* arr_merged = new int [sz_left+sz_right];
    // Add your code below this line. Do not modify any other code.
    int rindx = 0, lindx = 0, mindx=0;
    while (lindx < sz_left && rindx < sz_right)
    {
        if (arr_right[rindx] > arr_left[lindx])
            arr_merged[mindx++] = arr_left[lindx++];
        else 
            arr_merged[mindx++] = arr_right[rindx++];
    }
    if(lindx >= sz_left)
    {
        std::copy(arr_right + rindx, arr_right+sz_right, arr_merged + mindx);
    }
    else if(rindx >= sz_right)
    {
        std::copy(arr_left + lindx, arr_left + sz_left, arr_merged + mindx);
    }
        
    // Add your code above this line. Do not modify any other code.
    /* retun the merged array */
    return arr_merged;
}

int main()
{
    int v2[] = {2,5,7,8,9}, v1[] = {9};
    //int v1[] = {2}, v2[] = {};
    int szv1 = sizeof(v1)/sizeof(int);
    int szv2 = sizeof(v2)/sizeof(int);
    int * v = merge( v1, szv1, v2, szv2);
    for (int i = 0; i < szv1 + szv2; i++)
    {
        cout << v[i] << " ";
    }
    delete []v;
    return 0;
}
