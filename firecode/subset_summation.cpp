/// One of the hardest problems
#include <iostream>
#include <cstdlib>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
#include <map>
#include <algorithm>
#include <set>
using namespace std;
#define XX ? "True":"False"
// We generate all the possible subsets and save their sums
// The subset_sums contains sums for all the possible subsets
// For each number, if we add all items from subset_sums and
// push the summation into subset_sums
// Meaning for each number we double up the subset_sums. Hence, its 2^n
bool add_num_to_subsets(vector<int>& subset_sums, int cur, int target)
{
    if(cur == target) return true;
    int cursz = subset_sums.size();
    for(int i = 0 ; i < cursz; i++)
    {
        auto sum = subset_sums[i] + cur;
        if(sum == target) return true;
        subset_sums.push_back(sum);
    }
    subset_sums.push_back(cur);
    return false;
}
bool group_sum(int arr[], int size, int target)
{
    vector<int> subset_sums;
    for(int i = 0 ; i < size; i++)
    {
        if(add_num_to_subsets(subset_sums, arr[i], target)) return true;
    }

    return false;
}
int main()
{
    vector<int> v1={1,2,3,6,5};
    cout << ( group_sum(v1.data(), v1.size(), 18) XX) << endl;
    return 0;
}
