#include <iostream>
#include <string>
#include <cmath>
#include <cstring>
#include <vector>
using namespace std;
// Add any helper functions(if needed) above.
int* bubble_sort_array(int arr[], int size){
    for(int i = size - 1; i > 0; i--)
    {
        for (int j = 0; j < i ; j ++)
        if(arr [j] > arr[j + 1])
            std::swap(arr[j], arr[j+1]);
    }
    // Add your code above this line. Do not modify any other code.
    /* save the sorted array in int arr[] and return the same array */
    return arr;
}


int main()
{
    vector<int> v2 = {3,0};
    bubble_sort_array(v2.data(), v2.size());
    for(auto i : v2)
    {
        cout << i << " ";
    }
    return 0;
}