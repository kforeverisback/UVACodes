/// One of the hardest problems
#include <iostream>
#include <cstdlib>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
#include <map>
#include <iterator>
#include <algorithm>
#include <string>
using namespace std;

/*
Input: c a r
       c: add char 'c' to every string permutation possible with "ar", return the vector
         a: add char a to every string permutation possible with "r", return the vector
           r: since this is the last char, we'll return vector with "r" only, return the vector

Input: t e s t
       t: add char 't' to every permutation possible with "est", return the vector
         e: add char 'e' to every permutation possible with "st", return the vector
           s: add char 's' to every permutation possible with "t", return the vector
             t: since this is the last char, we'll return vector with "t" only
*/
vector<string> get_permutations(string input) 
{
    if(input.size() == 0 ) return vector<string>();
    vector<string> output;
    if(input.size() == 1) {
        output.push_back(input);
        return output;
    }
    char c = input[0];
    auto v = get_permutations(&input[1]);
    for (string s : v)
    {
        for(int i =0; i <= s.size(); i++)
        {
            string stemp(s);
            output.push_back(stemp.insert(i, 1, c));
        }
    }
    return output;
}

// vector<string> get_permutations2(string s) 
// {
//     if(s.size() == 0 || s[0] == '\0') return vector<string>();

//     vector<string> output;
//     for(int i = 0; i < s.size() ; i++)
//     {
//         output.push_back(s.insert())
//     }
// }

int main()
{
    string s = "abcd";
    auto v = get_permutations(s);
    std::copy(v.begin(), v.end(), std::ostream_iterator<string>(cout, " "));
    cout << endl;
    cout << v.size() << endl;
    return 0;
}