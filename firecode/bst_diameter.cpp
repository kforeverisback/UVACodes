#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
using namespace std;
#define XX ? "True":"False"
struct treeNode
{
    int value = -1;              /* value of the node */
    struct treeNode* left = nullptr;  /* pointer to the left child node */
    struct treeNode* right = nullptr; /* pointer to the right child node */
};
/**
 * Explanation: There are two possiblities for Dia
 * 1. The max distance is found in either left side or right side.
 *    In that case we need to calculate left side dia and right side dia
 *    Then chose the max of left and right
 * 2. Max dist is left side height + right side height + 1 (for root node)
 * With this two case, only one can be the max
 * Hence we calculate these two, and chose the max of (1) and (2)
*/
int dia_height(treeNode* node, int& height)
{
    if (node == nullptr) return 0;

    int h_left = 0, h_right = 0;
    // Calculate dia+height of left tree
    // For step 1
    int d_left = dia_height(node->left, h_left);
    // Calculate dia+height of right tree
    int d_right = dia_height(node->right, h_right);

    // We set the height of this node
    // 1 is for this current node
    // We calculate the height of this node, which is the max of
    // Left and right side + it's own
    height = std::max(h_left, h_right) + 1;
    // +1 is how leaf nodes are adding to their Dia and Height

    // Now for each node we calculate step 2
    // Determine which side has greatest length of dia
    int greatest_lr_dia = std::max(d_left, d_right);
    // The dia can be either the height of (lft and rght + 1 combined)
    // or the max of left side and right side dia
    return std::max(h_left + h_right + 1, greatest_lr_dia);
}
int diameter(treeNode* root)
{
    int height = 0;
    return dia_height(root, height);
}
int main()
{
    treeNode t[16]={0};
    t[0].value = 20;
    t[1].value = 15;
    t[2].value = 30;
    t[3].value = 14;
    t[4].value = 18;
    t[5].value = 35;
    t[6].value = 17;
    t[7].value = 19;
    t[8].value = 32;

    t[0].left = &t[1];
    t[0].right = &t[2];

    t[1].left = &t[3];
    t[1].right = &t[4];

    t[2].right = &t[5];
    t[2].left = nullptr;
 
    t[4].left = &t[6];
    t[4].right = &t[7];

    t[5].left = &t[8];
    t[5].right = nullptr;
   

    cout << (diameter(&t[0])) << endl;
    return 0;
}

/**
 * Elegant Solution!!!!!
pair<int, int> dia(treeNode* root) {
    if(!root) return {0, 0};
    
    auto l = dia(root->left);
    auto r = dia(root->right);
    return {
        max(l.first, r.first) + 1,
        max(l.first + r.first + 1, max(l.second, r.second))
    };
}
// Add any helper functions(if needed) above.
int diameter(treeNode* root)
{
    return dia(root).second;
}
*/