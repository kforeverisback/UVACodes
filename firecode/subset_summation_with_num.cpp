/// One of the hardest problems
#include <iostream>
#include <cstdlib>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
#include <map>
#include <algorithm>
#include <set>
using namespace std;
#define XX ? "True":"False"
/**
1 2 3
1 - 1     :0
2 - 1 2   :2  :C 1  : 0
3 - 1 2 3 :2 3:1 3  : 3 :C 1 2   :2  :1  : 0
**/
#define p(_X_) {std::copy( _X_.begin(), _X_.end(), std::ostream_iterator<int>(cout, " ") ); cout << endl;}
// Add any helper functions(if needed) above.
bool find_sum_subset(vector<int> cursum, const int *rest, int sz, int target)
{
    if( sz == 0 ) return false;

    bool ret = false;
    // std::transform(cursum.begin(), cursum.end(), std::back_inserter(cursum),
    //     [&](int val)
    //     {
    //         if(val + rest == target) ret = true;
    //         return val + rest;
    //     }
    // )
    vector<int> temp;
    for(int i = 0;i < cursum.size(); i++)
    {
        int s = cursum[i] + *rest;
        if(s == target)
            return true;
        temp.push_back(s);
    }
    std::copy(temp.begin(), temp.end(), std::back_inserter(cursum));
    return find_sum_subset(cursum, rest + 1, sz - 1, target);
}

bool group_sum_with_num(int arr[], int sz, int must_have, int target_sum)
{
    if (sz < 1) return false;

    vector<int> without_must;
    copy_if(arr, arr + sz, std::back_inserter(without_must), [must_have](int val){return val != must_have;});
    vector<int> temp;
    //temp.push_back(0);
    temp.push_back(must_have);
    return find_sum_subset(temp, without_must.data(), without_must.size(), target_sum);
}

int main()
{
    //vector<Interval> v1={{2,5},{1,3}};
    //vector<Interval> v1={{1,2}, {2,3}, {3,4}, {4,5}};
    //vector<Interval> v1={{-5,-3},{-4,-2},{0,10}};
    vector<int> v1={1,2,3,6,5};
    cout << ( group_sum_with_num(v1.data(), v1.size(), 3, 7) XX) << endl;
    return 0;
}
