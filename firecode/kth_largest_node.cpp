#include <iostream>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
using namespace std;

// Add any helper functions(if needed) below.
struct treeNode{
    int value;              /* value of the node */
    struct treeNode* left = nullptr;  /* pointer to the left child node */
    struct treeNode* right = nullptr; /* pointer to the right child node */
};

// Main Idea: We calculate each nodes Index.
// We do this by frist starting at the right most node, which is the first node (in terms of highest)
// Then we head back and calculate all others.
// Since right nodes are more important, when we found a right nore, we return it immediately
// Basically, current nodes index = size of right node + 1
treeNode* nth_node(treeNode* root, int target, int &i_am)
{
    if(!root || target == 0) return nullptr;
    treeNode* found_right = nth_node(root->right, target, i_am);
    if(found_right) return found_right;
    i_am += 1;
    if(i_am == target) return root;
    return nth_node(root->left, target, i_am);
}
treeNode* find_kth_largest(treeNode* root, int k) 
{
    int n = 0; // The rightmost node is the first node
    return nth_node(root, k, n);
}

int main()
{
    int sz = 20;
    treeNode tall[sz];
    for(int i = 1; i < sz; i++)
    {
        tall[i].value = i;
    }
    tall[6].left = &tall[4];
    tall[6].right = &tall[8];


    tall[4].left = &tall[3];
    tall[4].right = &tall[5];


    tall[8].left = &tall[7];
    tall[8].right = &tall[9];

    tall[3].left = &tall[2];
    //tall[3].right = &tall[8];
    for(int i = 1; i <= 10; i++)
    {
        auto p=find_kth_largest(&tall[6], i);
        cout << i; 
        if(p) cout << ": " << p->value << endl;
        else cout  << ": Not found!" <<endl;
    }
    return 0;
}

// int tree_size(treeNode* root)
// {
//     if(root == NULL) return 0;
//     return tree_size(root->left)+1+tree_size(root->right);
// }

// treeNode* find_kth_largest(treeNode* root, int k) 
// {
//     if (root == NULL) return NULL;
//     /* Track size of the right subtree */
//     int rightSize=0;
//     if (root->right != NULL)  /* Get the size of the right subtree */
//         rightSize = tree_size(root->right);
//     if (rightSize+1 == k)
//         return root;
//     else if (k <= rightSize) 
//         return find_kth_largest(root->right, k);
//     else
//         return find_kth_largest(root->left, k-rightSize-1);
// }