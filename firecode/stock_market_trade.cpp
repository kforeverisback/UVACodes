#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
using namespace std;
#define XX ? "True":"False"
int max_profit(int prices[], int sz)
{
    int sz_after = sz - (sz % 2);
    int profit = 0;
    for(int i = 0 ; i < sz_after; i+=2)
    {
        profit += prices[i + 1] - prices[i];
    }
    if (profit < 0) return 0;
    return  profit;
}

int main()
{
    auto v =vector<int>({50,100,20,80,20});
    cout << max_profit(v.data(), v.size()) << endl;
    v =vector<int>({0,100,0,100,0,100});
    cout << max_profit(v.data(), v.size()) << endl;
    v =vector<int>({100,40,20,10});
    cout << max_profit(v.data(), v.size()) << endl;
    v =vector<int>({0,50,10,100,30});
    cout << max_profit(v.data(), v.size()) << endl;
    return 0;
}