/// One of the hardest problems
#include <iostream>
#include <cstdlib>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
#include <map>
#include <iterator>
#include <algorithm>
#include <string>
using namespace std;
vector<string> string_combinations(queue<string> qs )
{
    if(qs.empty()) return vector<string>();
    string iam = qs.front();qs.pop();

    vector<string> output;
    if(qs.empty())
    {
        // base case
        for(char c : iam)
        {
            output.push_back(string(1, c));
        }
    }
    else
    {
        vector<string> next = string_combinations( qs );
        for( char c : iam )
        {
            for( auto ss : next )
            {
                output.push_back( string(1, c) + ss );
            }
        }
    }
    return output;
}
vector<string> get_strings_from_nums(string digits) 
{
    string maps[]={"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
    std::queue<string> digit_map_q;
    for(char c : digits)
    {
        digit_map_q.push(maps[c - '0']);
    }
    return string_combinations(digit_map_q);
}

int main()
{
    vector<string> digits={"34", "8", "232"};
    for(auto i : digits)
    {
        cout << i<< ":";
        auto v = get_strings_from_nums(i);
        std::copy(v.begin(), v.end(), std::ostream_iterator<string>(cout, " "));
        cout << endl;
    }
    return 0;
}