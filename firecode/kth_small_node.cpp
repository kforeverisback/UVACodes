#include <iostream>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
using namespace std;

// Add any helper functions(if needed) below.
struct treeNode{
    int value;              /* value of the node */
    struct treeNode* left = nullptr;  /* pointer to the left child node */
    struct treeNode* right = nullptr; /* pointer to the right child node */
    treeNode(){}
    treeNode(int n): value(n){}
    treeNode* operator=(int n)
    {
        this->value = n;
        return this;
    }
};
// We do indexing of each node.
// 
treeNode* find_indexed_node(treeNode* root, int k, int& nth)
{
    if(!root || k == 0) return nullptr;
    auto l = find_indexed_node(root->left, k, nth);
    // We don't find it in the left, search it in right
    if (l)
        return l;
    nth += 1;
    if ( nth == k )
        return root;
    return find_indexed_node(root->right, k, nth);
}
// We first the smallest node, then we index them one by one
treeNode* find_kth_smallest(treeNode* root, int k)
{
    int nth = 0;
    return find_indexed_node(root, k, nth);
}

#include <map>
int main()
{
    #define NULL_NODE INT32_MIN
    // INT32_MIN means NULL node
    vector<vector<int>> nodes = {
        {8, 3, 10},
        {3,1,6},
        {10, NULL_NODE, 14},
        {14,13, NULL_NODE},
        {6,4,7}
        };
    map<int, treeNode> ts;
    for(auto i : nodes)
    {
        if ( i[0] != NULL_NODE && ts.find(i[0]) == ts.end() )
            ts[i[0]] = i[0];
        if ( i[1] != NULL_NODE )
        {
            if(ts.find(i[1]) == ts.end()) ts[i[1]] =i[1];
            ts[i[0]].left  = &ts[i[1]];
        }
        if ( i[2] != NULL_NODE )
        {
            if(ts.find(i[2]) == ts.end()) ts[i[2]] =i[2];
            ts[i[0]].right = &ts[i[2]];
        }
    }
    //tall[3].right = &tall[8];
    for(int i = 1; i < 10; i++)
    {
        auto p=find_kth_smallest(&ts[8], i);
        cout << i; 
        if(p) cout << ": " << p->value << endl;
        else cout  << ": Not found!" <<endl;
    }
    return 0;
}

// int tree_size(treeNode* root)
// {
//     if(root == NULL) return 0;
//     return tree_size(root->left)+1+tree_size(root->right);
// }

// treeNode* find_kth_largest(treeNode* root, int k) 
// {
//     if (root == NULL) return NULL;
//     /* Track size of the right subtree */
//     int rightSize=0;
//     if (root->right != NULL)  /* Get the size of the right subtree */
//         rightSize = tree_size(root->right);
//     if (rightSize+1 == k)
//         return root;
//     else if (k <= rightSize) 
//         return find_kth_largest(root->right, k);
//     else
//         return find_kth_largest(root->left, k-rightSize-1);
// }