#include <iostream>
#include <string>
#include <cmath>
#include <cstring>
#include <vector>
using namespace std;
#define XX ? "True":"False"
struct listNode{
    int value = -1;               /* value of the node */
    struct listNode* next = nullptr;   /* points to the next linked-list node */
};

listNode* insert_at_position( listNode* head, int data,int pos)
{
    if (pos == 0) return head;
    if(pos == 1)
    {
        listNode* n = new listNode;
        n->value = data;
        n->next = head;
        return n;
    }
    head->next = insert_at_position(head->next, data, pos - 1);
    return head;
}

/// Own
listNode NODES[100];
listNode* c(std::vector<int> arr)
{
    if(arr.size() == 0 ) return nullptr;
    std::memset(&NODES[0], 0, sizeof(listNode) * 100);
    listNode* n = &NODES[0];
    for(int i = 0; i < arr.size(); i++)
    {
        NODES[i].value = arr[i];
        NODES[i].next = &NODES[i+1];
    }
    NODES[arr.size() - 1].next = nullptr;

    return &NODES[0];
}
void p(listNode* h)
{
    if(h == nullptr)
        cout << "{null}";
    while(h!=nullptr)
    {
        cout << h->value << "->";
        h = h->next;
    }
    cout << endl;
}
int main()
{
    listNode* r = c({1,2,3});
    // p(insert_at_position(r, 4, 2));
    r = c({9,1,4,2,3});
    p(insert_at_position(r, 8, 6));
    // r = c({1,4,2,3});
    // p(insert_at_position(r, 9, 1));
    return 0;
}