#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
using namespace std;
#define XX ? "True":"False"
// 2nd time better implementation
bool bin_s(int *arr, int left, int right, int n)
{
    int mid = (left + right ) / 2;
    if (arr[mid] == n) return true;
    if (left >= right) return false;
    
    if (arr[mid] > n)
        return bin_s(arr, left, mid - 1, n);
    else
        return bin_s(arr, mid + 1, right, n);
}
// Add any helper functions(if needed) above.
//bool binary_search(std::vector<int> arr, int size, int n)
bool binary_search(int arr[], int size, int n)
{
    // Add your code below this line. Do not modify any other code.                   
    if (size == 0) return false;
    return bin_s(arr, 0, size - 1, n);
    //return bin_s(arr.data(), 0, size - 1, n);
    //
}

/**
 * First time
 * Works Fine but the second one is better
 *
**/
/*
bool bin_search_rec(int* arr, int mid, int left, int right, int num)
{
    if(arr[mid] == num) return true;
    
    if (left == mid) return arr[right] == num;
    
    if(arr[mid] > num)
        return bin_search_rec(arr, left + (mid - left )/2, left, mid, num );
    else
        return bin_search_rec(arr, mid + (right - mid )/2, mid, right, num );
    
}
// Add any helper functions(if needed) above.
bool binary_search(int arr[], int size, int n)
{
    // Add your code below this line. Do not modify any other code.                   
    int left = 0;
    int right = size - 1;
    return bin_search_rec(arr, (right - left)/2, left, right, n);
    // Add your code above this line. Do not modify any other code.
}
*///
int main()
{
    int xx[] = {};
    cout << (binary_search( xx, 0,4) XX) << endl;
    // cout << (binary_search({2,8,9,12},4,6) XX) << endl;
    // cout << (binary_search({2},1,4) XX) << endl;
    // cout << (binary_search({},0,9) XX) << endl;

    return 0;
}

/**********************
 * Their Solution
***********************/

// bool binary_search(int arr[], int size, int n)
// {
//     /* Keep a track of middle element */
//     int mid = size/2;
//     /* Maintain boundary with first and last index */
//     int first = 0,last = size-1;
//     /* Iterate over the array from first to last element  
//     When first index exceeds last index, entire array is searched */
//     while(first <= last) {
//         mid = (first+last)/2;
//         /* If arr[mid] == n, Element Found */
//         if (arr[mid] == n) return true;
//         /* If arr[mid]<n, set first = mid+1 to divide the array, 
//         to look into second half part */
//         else if (arr[mid] < n) first = mid+1;
//         /* Otherwise search n into First half of the array, 
//         set first = mid+1 to divide the array */
//         else last = mid -1;
//     }
//     return false;
//     /