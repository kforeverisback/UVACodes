/*
You're given a game board that has m x n squares on it, represented by an m x n array. Write a method - count_paths that takes in m and n and returns the number of possible paths from the top left corner to the bottom right corner. Only down and right directions of movement are permitted.

Note:
Your method should output the result in a reasonable amount of time for large values of m and n. If you're thinking of using DFS, consider the tree depth and branching factor for m and n > 15!
*/
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
using namespace std;
#define XX  ?"True":"False"
// Soft DP Method
// int count_paths(int rows, int cols)
// {
//     int dp[rows+1][cols+1];
//     memset(dp,0,sizeof(dp));
//     dp[0][1] = 1;
    
//     for (int r=1;r<=rows;r++) for (int c=1;c<=cols;c++){
//         dp[r][c] = dp[r-1][c]+ dp[r][c-1];
//     }
    
//     return dp[rows][cols];
// }
// Best Solution
// int binomial_coefficient(unsigned int n, unsigned int k) {
//     long result = 1;
    
//     if (n-k < k) {
//         k = n-k;
//     }
//     for (unsigned int i = 0; i < k; ++i) {
//         result *= n-i;
//         result /= i+1;
//     }
    
//     return result;
// }
// // Add any helper functions(if needed) above.
// int count_paths(int rows, int cols)
// {
//     return binomial_coefficient(rows+cols-2, cols-1);
// }
int count_paths(int rows, int cols)
{
    // Lets assume we have a col max
    int cv[1024] = {1};
    // Just casually calculating Pascal's triangle...
    for (int r = 0; r < rows; r++)
    {
        for (int c=1; c < cols; c++)
        {
            cv[c] = cv[c - 1] + cv[c];
        }
    }

    // And returning the last value
    return cv[cols - 1];
}

int main()
{
    std::pair<int,int> vv = {1,2};
    vector<std::pair<int, int>> vals={ {1,1}, {7,15}, {18,17},{15,16},{2,2},{3,5},{8,10},{10,12},{5,3} };
    for (auto i : vals)
        cout << count_paths(i.first,i.second) << endl;
    return 0;
}