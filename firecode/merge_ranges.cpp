#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
// Some methods and signatures you'll find useful.
class Interval {
    public:
        int start;
        int end;
        Interval(){}
        Interval(int start, int end) {
            this->start = start;
            this->end = end;
        }
};
bool merge_one(vector<Interval>& v1, const Interval& i2)
{
    for (int ii = 0; ii <v1.size(); ii++)
    {
        Interval& i1 = v1[ii];
        if(i1.end >= i2.start){
            if (i1.end < i2.end) {
                i1.end = i2.end;
            }
            return true;
        }
    }
    return false;
}
// Add any helper functions(if needed) above.
vector<Interval> merge_intervals(vector<Interval> intervals_list)
{
    // Sort first
    auto cmp =[=](const Interval& i1, const Interval& i2) {
        return i1.start < i2.start;
    };
    //O(nLog(n))
    std::sort(intervals_list.begin(), intervals_list.end(), cmp);
    // Now we have smaller start index in front
    vector<Interval> output;
    //output.push_back(intervals_list[0]);
    // It might be better to remove it, oh well
    // Push the first
    //
    for(const Interval& i2 : intervals_list)
    {
        if ( !merge_one(output, i2) )
        {
            output.push_back(i2);
        }
    }

    // O(nLog(n))
    std::sort(output.begin(), output.end(), cmp);
    return output;
}

int main()
{
    //vector<Interval> v1={{2,5},{1,3}};
    //vector<Interval> v1={{1,2}, {2,3}, {3,4}, {4,5}};
    //vector<Interval> v1={{-5,-3},{-4,-2},{0,10}};
    vector<Interval> v1={{0,1},{0,1},{0,1},{0,0}};
    auto kk = merge_intervals(v1);
    for (auto i : kk)
    {
        cout << i.start << "," << i.end << endl;
    }
    return 0;
}