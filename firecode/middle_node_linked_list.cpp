#include <iostream>
#include <string>
#include <cmath>
#include <cstring>
#include <vector>
using namespace std;
#define XX ? "True":"False"
struct listNode{
    int value = -1;               /* value of the node */
    struct listNode* next = nullptr;   /* points to the next linked-list node */
};

// Awesome solution with tortoise and hare
// Or one 2x fast pointer another slow pointer
// listNode* find_middle_node(listNode* head) 
// {   /* Initialise two pointers pointing to the head of the linked list */
//     listNode* slow = head;
//     listNode* fast = head;
//     /* Traverse the list by moving one pointer with double speed than other pointer.
//      Stop when the fastest pointer reaches the end of linked list. 
//      The slower pointer will be at the middle of the linked list */
//     while(fast && fast->next != NULL && (fast->next)->next != NULL  ) {
//         slow = slow->next;
//         fast = (fast->next)->next;
//     }
//     return slow;                /* Return the middle node */
// }

listNode* mid_node_nth(listNode* head, int my_cnt, int& ret_length)
{
    // Once we reach the end, head will be nullptr
    // Length will be cnt+1
    // That is for 1 node, it will be 2, for 5 nodes ret_len will be 6
    if(head == nullptr) 
    {
        ret_length = my_cnt;
        return head;
    }
    listNode* n = mid_node_nth(head->next, my_cnt + 1, ret_length);
    // Now if this node is at the middle return this, otherwise return null
    if ( (ret_length) / 2 == my_cnt )
        return head;
    
    return n;
}

listNode* find_middle_node(listNode* head)
{
    int ret_len = -1;
    return mid_node_nth(head, 1, ret_len);
}

/// Own
listNode NODES[100];
listNode* c(std::vector<int> arr)
{
    if(arr.size() == 0 ) return nullptr;
    std::memset(&NODES[0], 0, sizeof(listNode) * 100);
    listNode* n = &NODES[0];
    for(int i = 0; i < arr.size(); i++)
    {
        NODES[i].value = arr[i];
        NODES[i].next = &NODES[i+1];
    }
    NODES[arr.size() - 1].next = nullptr;

    return &NODES[0];
}
void p(listNode* h)
{
    if(h == nullptr)
        cout << "{null}";
    while(h!=nullptr)
    {
        cout << h->value << "->";
        h = h->next;
    }
    cout << endl;
}
int main()
{
    listNode* r = c({1,2,3});
    // p(insert_at_position(r, 4, 2));
    r = c({1});
    cout << find_middle_node(r)->value << endl;
    r = c({1,2});
    cout << find_middle_node(r)->value << endl;
    r = c({1,2,3});
    cout << find_middle_node(r)->value << endl;
    r = c({1,2,3,4});
    cout << find_middle_node(r)->value << endl;
    r = c({1,2,3,4,5});
    cout << find_middle_node(r)->value << endl;

    // r = c({1,4,2,3});
    // p(insert_at_position(r, 9, 1));
    return 0;
}