#include <iostream>
#include <string>
#include <cmath>
using namespace std;

// Add any helper functions(if needed) above.
int* selection_sort_array(int arr[], int size){
    for( int i = 0; i < size; i++)
    {
        int* cur = &arr[i];
        int* swapable = cur;
        for (int j=i;j < size; j++)
        {
            cout << arr[j] << endl;
            if (arr[j] < *swapable)
                swapable = &arr[j];
        }

        if(swapable != cur) std::swap(*cur, *swapable);
    }
    // Add your code above this line. Do not modify any other code.
    /* save the sorted array in int arr[] and return the same array */
    return arr;
}

void print(int arr[], int sz)
{
    for (int i = 0;i < sz;i++) {printf("%d ", arr[i]);}
    printf("\n");
}
int main()
{
    int arr[] = {52,3,2};
    int size = sizeof(arr) / sizeof(int);
    selection_sort_array(arr, size);
    print(arr, size);
    return 0;
}