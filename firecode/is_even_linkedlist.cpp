#include <iostream>

struct listNode {
    int value;
    struct listNode* next;
};
bool is_even_list(listNode* head)
{
    if(head == nullptr) return true;

    return is_even_list(head->next) == false;
}
