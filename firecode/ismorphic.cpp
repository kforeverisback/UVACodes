#include <iostream>
#include <string>
#include <cmath>
using namespace std;
#define XX  ?"True":"False"
// namespace std have been included for this problem.

// Add any helper functions(if needed) above.
bool is_isomorphic(string input1, string input2)
{
    if (input1.size() != input1.size()) return false;

    char translation1[256] = {0};
    char translation2[256] = {0};
    //cout << "1:" << input1 << ", 2:" << input2 << endl;
    for ( int i = 0; i < input1.size(); i++ )
    {
        if (translation1[input1[i]] == 0 && translation2[input2[i]] == 0)
        {
            translation1[input1[i]] = input2[i];
            translation2[input2[i]] = input1[i];
        }
        else if (translation1[input1[i]] != input2[i] || translation2[input2[i]] != input1[i])
        {
            return false;
        }
    }

    return true;
}

#include <vector>
int main()
{
    vector<string> s1 = {"abcd","firecode", "","pro", "firecode", "add", "css", "css", "abcabc", "abcabc"};
    vector<string> s2 = {"aabb","firecoat", "","pro", "firec",    "egg", "dll", "dle", "xyzxyz", "xbexyz"};
    for ( int i = 0; i < s1.size(); i++ )
    {
        cout << (is_isomorphic(s1[i], s2[i]) XX) << endl;
    }
    return 0;
}