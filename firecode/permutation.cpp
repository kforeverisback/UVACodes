#include <iostream>
#include <string>
#include <cmath>
using namespace std;
#define TF(_X_) ((_X_) == true? "True":"False")
// Add any helper functions(if needed) above.
bool permutation(string input1, string input2)
{
    cout << input1 << ","<<input2 << endl;
    if (input1.size() != input2.size()) return false;
    //assume ascii strings
    int alpha[256]={0};
    for ( int i = 0; i < input1.size() ; i ++)
    {
        alpha[(uint8_t)input1[i]]++;
        alpha[(uint8_t)input2[i]]--;
    }

    int64_t total = 0;
    for(auto i : alpha)
    {
        if (i != 0) return false;
    }

    return true;
}

int main()
{
    cout <<TF(permutation("CATANA","CATTAN")) << endl;
    cout <<TF(permutation("NOSZE","NOSZE2")) << endl;
    cout << TF(permutation("CAT","ACT")) << endl;
    cout << TF(permutation("hello","aloha")) << endl;
    cout <<TF(permutation("DoYouDo","oDouYDo")) << endl;
    cout <<TF(permutation("Bother","therBo")) << endl;

    return 0;
}