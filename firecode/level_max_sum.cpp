#include <iostream>
#include <numeric>
#include <vector>
#include <queue>
#include <cstring>
using namespace std;

// Add any helper functions(if needed) below.
struct treeNode{
    int value;              /* value of the node */
    struct treeNode* left = nullptr;  /* pointer to the left child node */
    struct treeNode* right = nullptr; /* pointer to the right child node */
};

// Add any helper functions(if needed) above.
int find_max_sum_level(treeNode* root)
{
    if (root == nullptr ) return -1;

    int ms = 0, max_level=0, cur_level = 0, cur_sum = 0;
    queue<pair<treeNode*,int>> qn;
    qn.push(make_pair(root, 0));
    while(qn.size() > 0 )
    {
        treeNode* n = qn.front().first;
        int cl = qn.front().second;
        qn.pop();
        if (ms < cur_sum)
        {
            max_level = cur_level;
            ms = cur_sum;
        }
        if(cur_level != cl)
        {
            cur_level = cl;
            cur_sum = 0;
        }
        cur_sum += n->value;
        if(n->left)
            qn.push(make_pair(n->left, cl + 1) );
        if(n->right)
            qn.push(make_pair(n->right, cl+1));

        cur_level = cl;
    }

    return max_level;
}

int main()
{
    int sz = 20;
    treeNode tall[sz];
    for(int i = 1; i < sz; i++)
    {
        tall[i].value = i;
    }
    tall[1].left = &tall[2];
    tall[1].right = &tall[3];


    tall[2].left = &tall[4];
    tall[2].right = &tall[5];
    tall[1].value = 200;

    tall[3].left = &tall[6];
    tall[3].right = &tall[7];

    tall[4].right = &tall[8];
    tall[4].left = &tall[9];

    tall[5].left = &tall[10];
    tall[5].right = &tall[11];
    cout <<  find_max_sum_level(&tall[1]) << endl;
    return 0;
}
