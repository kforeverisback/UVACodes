#include <iostream>
#include <string>
#include <cmath>
#include <cstring>
#include <vector>
using namespace std;
#define XX ? "True":"False"
struct listNode{
    int value = -1;               /* value of the node */
    struct listNode* next = nullptr;   /* points to the next linked-list node */
};
// Add any helper functions(if needed) above.
listNode* delete_at_pos(listNode* head, int n)
{
    if (head == nullptr)
        return nullptr;
    if(n == 1)
        return head->next;

    head->next = delete_at_pos(head->next, n - 1);
    return head;
}

/// Own
listNode NODES[100];
listNode* c(std::vector<int> arr)
{
    if(arr.size() == 0 ) return nullptr;
    std::memset(&NODES[0], 0, sizeof(listNode) * 100);
    listNode* n = &NODES[0];
    for(int i = 0; i < arr.size(); i++)
    {
        NODES[i].value = arr[i];
        NODES[i].next = &NODES[i+1];
    }
    NODES[arr.size() - 1].next = nullptr;

    return &NODES[0];
}
void p(listNode* h)
{
    if(h == nullptr)
        cout << "{null}";
    while(h!=nullptr)
    {
        cout << h->value << "->";
        h = h->next;
    }
    cout << endl;
}
int main()
{
    listNode* r = c({1,2,3,4,5});
    p(delete_at_pos(r, 3));
    r = c({1,2,3,4,5});
    p(delete_at_pos(r, 5));
    r = c({1,2,3,4,5});
    p(delete_at_pos(r, 1));
    r = c({1});
    p(delete_at_pos(r, 3));
    p(delete_at_pos(r, 5));
    p(delete_at_pos(r, 1));
    r = c({});
    p(delete_at_pos(r, 3));
    p(delete_at_pos(r, 1));
    r = c({1,8});
    p(delete_at_pos(r, 1));
    r = c({1,6});
    p(delete_at_pos(r, 2));
    return 0;
}