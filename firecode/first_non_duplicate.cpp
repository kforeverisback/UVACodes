#include <iostream>
#include <string>
#include <cmath>
#include <queue>
using namespace std;
#define XX ? "True":"False"
// Add any helper functions(if needed) above.
char first_non_repeating(string str)
{
    char first='0';
    int map[256]={0};
    queue<char> ones;
    for (int i = 0; i < str.size(); i++)
    {
        map[str[i]]++;
        if (map[str[i]] == 1)
        {
            if (first == '0')
                first = str[i];
            else
                ones.push(str[i]);
        }
        else if(map[str[i]] > 1 && first == str[i])
        {
            first = ones.empty()?'0':ones.front();
            ones.pop();
        }
    }

    return first;
}

int main()
{
    cout << (first_non_repeating( "abcdcd" ) ) << endl;
    cout << (first_non_repeating( "cbcd" ) ) << endl;
    cout << (first_non_repeating( "cdcd" ) ) << endl;
    cout << (first_non_repeating( "aaaa" ) ) << endl;
    cout << (first_non_repeating( "" ) ) << endl;
    cout << (first_non_repeating( "abcdcd" ) ) << endl;
    return 0;
}