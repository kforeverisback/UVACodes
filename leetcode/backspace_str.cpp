#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <numeric>
#define XX ? "True":"False"
using namespace std;
#include <array>
array<char, 201> getArr(string S)
{
    array<char, 201> sarr={0};
    int sindx = 0;
    for(int i = 0 ; i < S.size(); i++)
    {
        char& c = S.at(i);
        if(c == '#')
        {
            if(sindx > 0) sindx--;
        }
        else
        {
            sarr[sindx++] = c;
        }
    }
    // In case our last one is a #, we need to set it to zero
    // Later we will check whether both are zero or not
    sarr[sindx]=0;
    return sarr;
}
bool backspaceCompare(string S, string T) {
    auto sarr=getArr(S);
    auto tarr=getArr(T);
    // for(int i = 0 ; i < sarr.size() && sarr[i] != 0; i++){cout <<sarr[i];}
    // cout << endl;
    // for(int i = 0 ; i < tarr.size() && tarr[i] != 0; i++){cout <<tarr[i];}
    // cout << endl;
    for(int i = 0 ; i < sarr.size(); i++)
    {
        if(sarr[i] != tarr[i]) return false;
        if(sarr[i] == 0 && tarr[i] == 0) break;
    }

    return true;
}


int main()
{
    vector<pair<string,string>> v = {
        {"a#c","b"},
        {"ab##","c#d#"},
        {"a##c","a#c#"},
        {"a##c","#a#c"},
        {"a##bc#","b"},
        {"abc#cd##","ba"},
        {"abc#cd##","ab"},
        {"a","#"},
        {"a","b"},
        {"####a","b###b"},
        {"#","#"},
        {"#aaaa","aaaaa#"},

    };
    for (auto i : v)
    {
        //cout << "B:";
        cout <<"S: "<<i.first << ", T: " << i.second << endl;//" = ";
        cout << (backspaceCompare(i.first, i.second) XX ) << endl;
    }
    return 0;
}