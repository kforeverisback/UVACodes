#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <numeric>
#define XX ? "True":"False"
using namespace std;
class Solution {
public:
    int maxProfit(vector<int>& prices) {
        // Just check the ascending
        if(prices.size() < 2) return 0;
        int profit = 0;
        for(int i = 1 ; i < prices.size(); i ++)
        {
            if(prices[i] > prices[i - 1])
                profit += prices[i] - prices[i - 1];
        }

        return profit;
    }
};

int main()
{
    vector<vector<int>> v = {
        {5,1,1,1,1,9},
        {2,1,1,1,1,6},
        {1},
        {2,1},
        {1,2},
        {7,1,5,3,6,4},
        {1,2,3,4,5},
        {7,6,4,3,1},
        {1,2,3,4,5,5,4,3,2,1},
        {6,5,4,3,1,2,3,4,5,7}
    };
    for (auto i : v)
    {
        //cout << "B:";
        std::copy(i.begin(), i.end(), ostream_iterator<int>(cout, " "));
        cout << ":" << Solution().maxProfit(i);
        cout << endl;
    }
    return 0;
}