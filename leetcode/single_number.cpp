#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
using namespace std;
int singleNumber(vector<int>& nums) {
    unordered_map<int, int> mp;
    for(auto i : nums)
    {
        mp[i]++;
    }
    for(auto i : mp)
    {
        if(i.second == 1) return i.first;
    }
    
    return 0;
}
#include <unordered_set>
int singleNumber_math(vector<int>& nums) {
    // This solution takes advantage of the fact that:
    // 2 * (a+b+c) - ( a+a + b+b + c) = c
    // So we first generate unique numbers using unordered_set
    // Which is O(logn) complexity of searching
    unordered_set<int> ss;
    auto v = std::accumulate(nums.begin(), nums.end(), 0,[&ss](int db, int n){
        ss.insert(n);
        return db+n;
    });
    //2 * (a+b+c) - ( a+a + b+b + c) = c
    cout << 2*std::accumulate(ss.begin(), ss.end(), 0) - v << endl;
}
int main()
{
    cout << "TEST" << endl;
    //vector<int> v2 = {4,1,2,1,2};
    //cout << singleNumber(v2) << endl;
    return 0;
}