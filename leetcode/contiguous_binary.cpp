#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <stack>
#include <numeric>
#include <unordered_map>
#define XX ? "True":"False"
using namespace std;
int findMaxLength(vector<int>& nums) {
        if(nums.size() < 2) return 0;
        // We add -1 for 0 and +1 for 1. In that way the cumulative sum will be zero if 1/0 have same count
        // We use map to save the index for a cumulative sum
        unordered_map<int,int> prefix;
        prefix[0] = -1; // Special case, when the sum is zero we should save the max of indices
        int cumsum = 0, maxarrsz = -1;
        for(int i = 0 ; i < nums.size(); i++)
        {
            cumsum += (nums[i] == 0? -1 : 1);
            if(prefix.count(cumsum) == 0) // We don't have that sum value
            {
                prefix[cumsum] = i;
            }
            else// We have that sum! 
            {
                maxarrsz = max (maxarrsz, i - prefix[cumsum]);
            }
        }
        return maxarrsz;
    }
int main()
{
    vector<vector<int>> v = {
        {0,1},
        {0,1,1,0,0,0,1,0,1,1},
        {0,0,1,0,1,0,1,1,0,1,0,0,0,1,1,1},
        {0,1,0,0,0,1,1,0,0,0},
        {0,1,1,1,0,0},
        {},
        {1},
        {0,1,1,1,0,1,1,1,1,0,0,0},
        {0,1,1,1,1,1,0,0},
        {0,1,1,0,1,0,0,0},
    };
    for (auto i : v)
    {
        //cout << "B:";
        std::copy(i.begin(), i.end(), ostream_iterator<int>(cout, " "));
        cout << endl << findMaxLength(i) << endl;
        cout << endl;
    }
    return 0;
}