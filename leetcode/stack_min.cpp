#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <map>
#include <iterator>
#include <numeric>
using namespace std;
#define XX ? "True":"False"
class MinStack2 {
    vector<pair<int,int>> _starr;
    //int _curindx;
public:
    /** initialize your data structure here. */
    MinStack2() {
        _starr.reserve(64);
    }
    
    void push(int x) {
        // if (_starr.size() + 1 >= _starr.capacity())
        //     _starr.resize(_starr.max_size() * 2); // Exponential growth
        
        pair<int, int> p;
        p.first = x;
        if(_starr.size() > 0)
        {
            int min = getMin();
            p.second = std::min(min, x);
        }
        else
        {
            p.second = x;
        }
        _starr.push_back(p);
    }
    
    void pop() {
        // if (_starr.size() < _starr.max_size() / 4)
        //     _starr.resize(_starr.max_size() / 2 ); // O(logN)
        _starr.pop_back();
    }
    
    int top() {
        return _starr.back().first;
    }
    
    int getMin() {
        return _starr.back().second;
    }
};

#include <stack>
class MinStack {
    stack<pair<int,int>> _st;
    //int _curindx;
public:
    /** initialize your data structure here. */
    MinStack() {}
    
    void push(int x) {
        pair<int, int> p;
        p.first = x;
        if(_st.size() > 0) {
            int min = getMin();
            p.second = std::min(min, x);
        } else {
            p.second = x;
        }
        _st.push(p);
    }
    
    void pop() { _st.pop(); }
    
    int top() const { return _st.top().first; } 
    
    int getMin() const { return _st.top().second; }
};

int main()
{
    vector<int> vv(2);
    auto v = vv.back();
    vv.pop_back();
    vector<char> ops={'h','o','h','t','h','m','t','h','o','m','t','o','h','m','t','o','m','t',};
    vector<int> vals={20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1};
    MinStack obj;
    for(char c : ops)
    {
        switch(c){
            case 'h':
            {
                int val = vals.back();
                vals.pop_back();
                obj.push(val);
                cout << "Op: " << c << ", val:" << val << ", top: " << obj.top() << ", min: " << obj.getMin() << endl;
            }
            break;
            case 'o':
            {
                obj.pop();
                cout << "Op: " << c << ", top: " << obj.top() << ", min: " << obj.getMin() << endl;
            }
            break;
            case 't':
            {
                cout << "Op: " << c << ", top: " << obj.top() << ", min: " << obj.getMin() << endl;
            }
            break;
            case 'm':
            {
                cout << "Op: " << c << ", top: " << obj.top() << ", min: " << obj.getMin() << endl;
            }
            break;
            default:
            cout << "Not possible" << endl;
            break;
        }
    }
}