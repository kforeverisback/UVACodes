#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <numeric>
#define XX ? "True":"False"
using namespace std;
int lastStoneWeight(vector<int>& stones) {
    if(stones.size() < 2) return stones[0];

    // Lets sort first, O(nLogn)
    std::sort(stones.begin(), stones.end());
    //std::copy(stones.begin(), stones.end(), ostream_iterator<int>(cout, " "));cout << endl;
    // We're not doing descending because, it's easier to remove items from back
    while(stones.size() > 1)
    {
        int largest = stones.back();
        stones.pop_back();

        int diff = largest - stones.back();
        stones.pop_back(); // We pop back
        if(diff > 0 ) { // Then push the diff at the end

            stones.push_back(diff);
            // And bubble up as required
            for(int cur = stones.size() - 2; cur >= 0; cur --)
            {
                if(stones[cur] > stones[cur + 1]) std::swap(stones.at(cur+1), stones.at(cur));
                else break; // Once we stopped swapping, it means we're done
            }
        }
    }
    if(stones.size() == 0) return 0;
    
    return stones[0];
}
/*
[1,8,4,3,9,3,2,5] -> 9,8 --> [1,1,4,3,3,2,5]
[1,1,4,3,3,2,5] -> 5,4 --> [1,1,1,3,3,2]
*/
int main()
{
    vector<vector<int>> v = {
        {1},
        {2,7,4,1,8,1},
        {2,2},
        {1,8,4,3,9,3,2,5},
        {1,6,6,2,3,4,6,8,7,9,7,8},
        {60,80,88,90,36,100,66,21},
        {2,1},
    };
    for (auto i : v)
    {
        //cout << "B:";
        std::copy(i.begin(), i.end(), ostream_iterator<int>(cout, " "));
        cout << ":" << lastStoneWeight(i) << endl;
        cout << endl;
    }
    return 0;
}