#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <numeric>
#define XX ? "True":"False"
using namespace std;
#include <array>
int countElements(vector<int>& arr) {
    array<bool, 1024> cntarr={false};
    for(int i = 0 ; i <  arr.size(); i++)
    {
        int& ii = arr.at(i);
        if(!cntarr[ii]) cntarr[ii] = true;
        ii++;
    }

    int cnt = 0;
    for(int i = 0 ; i <  arr.size(); i++)
    {
        if(cntarr[arr.at(i)]) cnt++;
    }
    return cnt;
}

int main()
{
    vector<vector<int>> v = {
        {1,2,3},
        {1,1,3,3,5,5,7,7},
        {1,3,2,3,5,0},
        {1,1,2,2},
        {0},
        {1,1,1,1,1,1,1,1},
        {1,1,1,1,1,1,1,2}
    };
    for (auto i : v)
    {
        //cout << "B:";
        std::copy(i.begin(), i.end(), ostream_iterator<int>(cout, " "));
        cout << ":" << countElements(i);
        cout << endl;
    }
    return 0;
}