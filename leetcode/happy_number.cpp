#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#define XX ? "True":"False"
using namespace std;
int singleNumber(vector<int>& nums) {
    unordered_map<int, int> mp;
    for(auto i : nums)
    {
        mp[i]++;
    }
    for(auto i : mp)
    {
        if(i.second == 1) return i.first;
    }
    
    return 0;
}
#include <unordered_set>
int sq[]={0,1,4,9,16,25,36,49,64,81};
bool isHappyHelper(int n, unordered_set<int>& looptest) {
    if (n == 0) return false;
    if (n == 1) return true;

    int sum = 0;
    cout << n << ":";
    while(n != 0){
        int r = n % 10;
        n = n / 10;
        sum += sq[r];
    }

    cout << sum << ", ";
    if(sum == 1) return true;
    if(looptest.find(sum) != looptest.end()) return false;
    looptest.insert(sum);

    return isHappyHelper(sum, looptest);
}
bool isHappy(int n) {
    unordered_set<int> looptest;
    return isHappyHelper(n, looptest);
}
#include <iterator>
int main()
{
    // for(auto i : {0,1,2,3,4,5,6,7,8,9})
    // {
    //     for(auto j : {1,2,3,4,5,6,7,8,9})
    //     {
    //         int cnt = 1, sum = 0, cnt_loop = 0;
    //         int n = (i * 10 + j);
    //         unordered_set<int> test;
    //         bool loop = false;
    //         while(cnt <= 100 && n != 1)
    //         {
    //             while(n != 0){
    //                 int r = n % 10;
    //                 n = n / 10;
    //                 sum += sq[r];
    //             }
    //             if(test.find(sum) != test.end() && !loop)
    //             {
    //                 loop = true;
    //                 cnt_loop = cnt;
    //             }
    //             test.insert(sum);
    //             //test[sum] = true;
    //             n = sum; cnt++;
    //             sum = 0;
    //         }
    //         cout << i<<j<<" S:" << sum << ", c:" << cnt  <<", Loop: " << loop << ", c: " << cnt_loop << endl; 
    //     }
    // }
    // vector<int> v(10);
    // std::generate(v.begin(), v.end(), [n = 0] () mutable { return n++; });
    vector<int> v = {19, 828, 121,441,112};
    for (auto i : sq)
    {
        cout << i << ":" << (isHappy(i) XX) << endl;
    }
    return 0;
}