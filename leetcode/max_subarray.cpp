#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#define XX ? "True":"False"
using namespace std;
/*
Sample Solution
int maxSubArray(vector<int>& nums) {
        if (nums.size() == 0) return 0;
        int max_sum = nums[0];
        int cur_sum = nums[0];
        
        for (int i = 1; i < nums.size(); ++i) {
            cur_sum += nums[i];
            if (cur_sum < nums[i]) {
                cur_sum = nums[i];
            }
            max_sum = max(max_sum, cur_sum);
        }
        
        return max_sum;
    }
*/
int maxSubArray(vector<int>& nums) {
    if( nums.size() == 0 ) return 0;
    // By putting, max = MINIMUM we guarantee max can be negative if required
    int max_sum = INT32_MIN, cur_sum = 0;
    for(int i = 0 ; i < nums.size(); i ++)
    {
        cur_sum += nums[i];
        if(cur_sum < 0) {
            cur_sum = 0;
            max_sum = std::max(max_sum, nums[i]);
        }
        else {
            max_sum = std::max(max_sum, cur_sum);
        }
    }

    return max_sum;
}
int main()
{
    vector<vector<int>> v = {
        {1},
        {-1},
        {-1,5,10,-1,-6,-2,-5},
        {-10,-4,-5,-2,-1,-3},
        {-2,1,-3,4,-1,2,1,-5,4}
    };
    for (auto i : v)
    {
        cout << maxSubArray(i) << endl;
    }
    return 0;
}