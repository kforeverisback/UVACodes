#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <numeric>
#define XX ? "True":"False"
using namespace std;
void moveZeroes(vector<int>& nums) {
    if(nums.size() < 2) return;
    int zero_idx = -1;
    // Get the first zero
    for(zero_idx = 0; zero_idx < nums.size() || nums[zero_idx] != 0; zero_idx++);
    cout << endl << zero_idx << endl;
    //while(nums.size() > zero_idx && [++zero_idx] != 0);
    for(int cur = zero_idx + 1; cur < nums.size(); cur++)
    {
        // If zero then continue upto the next
        // Increasing cur indx, but keeping zero indx
        if(nums.at(cur) == 0) continue;
        else {
            std::swap(nums.at(cur),nums.at(zero_idx));
            zero_idx++;
        }
    }
}

int main()
{
    vector<vector<int>> v = {
        {1},
        {0},
        {2,1},
        {1,2,3,4,4},
        {0,1,4,2,0,1,0,1},
        {0,0,0,1,0,2,0,3,4,0,0,4},
        {1,2,3,4,0,0,4},
        {1,2,4,5,6,7,0}
    };
    for (auto i : v)
    {
        //cout << "B:";
        std::copy(i.begin(), i.end(), ostream_iterator<int>(cout, " "));
        moveZeroes(i);
        cout << endl;
        //cout << "A:";
        std::copy(i.begin(), i.end(), ostream_iterator<int>(cout, " "));
        cout << endl;
    }
    return 0;
}