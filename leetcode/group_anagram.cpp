#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <numeric>
#define XX ? "True":"False"
using namespace std;
#include <unordered_map>
#include <unordered_set>

vector<vector<string>> groupAnagrams(vector<string>& strs) {
    vector<vector<string>> grps;
    unordered_map<string,int> grp_map;
    for(string s : strs)
    {
        string orig = s;
        std::sort(s.begin(), s.end());
        auto it = grp_map.find(s);
        if( it == grp_map.end() )
        {
            // Create a new group
            vector<string> v;v.push_back(orig);
            grps.push_back(v);
            grp_map[s] = grps.size() - 1;
        }
        else
        {
            // We found it
            grps[grp_map[s]].push_back(orig);
        }
    }
    return grps;
}
int main()
{
    vector<vector<string>> v = {
        {"eat", "tea", "tan", "ate", "nat", "bat"}
    };
    for (auto i : v)
    {
        auto r = groupAnagrams(i);
        cout << "Arr:\n" ;
        std::copy(i.begin(), i.end(), ostream_iterator<string>(cout, ","));
        cout << endl << "Soln:\n";
        for(auto ri : r)
        {
        std::copy(ri.begin(), ri.end(), ostream_iterator<string>(cout, ","));
        cout << endl;
        }
    }
    return 0;
}