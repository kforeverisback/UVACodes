#include <iostream>
#include <vector>
using namespace std;
/*
 * Original solution
 * The idea is to generate multiplication from left and right
 * e.g.
 * Original Arr                             : 1  2  3  4
 * Left Mult Arr [i+1]= left[i]    * num[i] : 1  1  2 6 24
 *                                             \  \  \ \
 *                                              \  \  \ \
 *                                               \  \  \ \
 * Right Mult Arr [i] = right[i+1] * num[i] : 24 24 12 4 1
 *                                              /  / / /
 * Final array left[i] * right [i+1]        : 24 12 8 6
 * vector<int> productExceptSelf(vector<int>& nums) {
        int n = nums.size();
        int left[n + 1], right[n + 1];
        left[0] = 1;
        for(int i = 0; i < n; i++){
            left[i + 1] = left[i] * nums[i];
        }
        right[n] = 1;
        for(int i = n - 1; i >= 0; i--){
            right[i] = right[i + 1] * nums[i];
        }
        vector<int> ans;
        for(int i = 0; i < n; i++){
            ans.push_back(left[i] * right[i + 1]);
        }
        return ans;
    }
*/
// This is a O(n) recursive solution, with O(1) space
int allOthersMult(int curIndx, pair<int,int> prevMult, const vector<int>& nums, vector<int>& output)
{
    //pair<int,int> prevMult
    // First one is multiplication with prev number
    // Second one is multiplication without prev number
    if (curIndx >= nums.size()) {
        return 1;
    }
    pair<int,int> mult;
    mult.first = prevMult.first*nums[curIndx];
    mult.second= prevMult.first;
    int ret = allOthersMult(curIndx + 1, mult, nums, output);
    output[curIndx] = ret * prevMult.first;
    return ret * nums[curIndx];
}

vector<int> productExceptSelf(vector<int>& nums) {
    vector<int> output(nums.size(), 0); // Initiate the output vector
    cout << "Sz: " << output.size() << endl;
    allOthersMult(0, {1, 1}, nums, output);
    return output;
}

int main()
{
    vector<vector<int>> v = {
        {1,2,3,4},
    };
    for (auto i : v)
    {
        //cout << "B:";
        std::copy(i.begin(), i.end(), ostream_iterator<int>(cout, " "));
        cout << ":";
        auto out = productExceptSelf(i);
        std::copy(out.begin(), out.end(), ostream_iterator<int>(cout, " "));
        cout << endl;
    }
    return 0;
}