#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
using namespace std;
//Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x = 0) : val(x), next(NULL) {}
    ListNode* operator=(int x) {
        this->val=x ;
        return this;
    }
};

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2, int carry = 0) {
        int val = carry;
        ListNode* l1n = nullptr;
        if(l1) {
            val += l1->val;
            l1n = l1->next;
        }
        ListNode* l2n = nullptr;
        if(l2) {
            val += l2->val;
            l2n = l2->next;
        }
        // Add it one time
        ListNode* n = nullptr;
        // If either l1 or l2 node is available create a node
        // Or is carry is available then  also create a node
        if(l1 || l2 || val) {
            n = new ListNode(val % 10);
            n->next = addTwoNumbers(l1n, l2n, val / 10);
        }

        return n;
    }
};

void verify_sum(ListNode* n1, ListNode* n2, ListNode* r)
{
    int n1s = 0, n2s = 0, rs = 0;
    auto sum = [](ListNode* l1) ->int
    {
        int mult = 1, sum = 0;
        while(l1){
            sum += l1->val * mult;
            mult *=10;
            l1 = l1->next;
        }
        return sum;
    };
    auto a = sum(n1), b = sum(n2), rc = a + b;
    cout << sum(n1) << "+"<<sum(n2) << "=";
    if(r){ cout<< sum(r);}
    else {cout <<"Result is null";}
    cout << " > should be:" << rc<<endl;
}
int main()
{
    vector<vector<ListNode>> nn1 = {{5}, {2,4,3}, {0}, {5,3  },{0     }};
    vector<vector<ListNode>> nn2 = {{5}, {5,6,4}, {0}, {5,6,4}, {5,6,4}};
    for(vector<ListNode>& n1:nn1)
    {
        for(int i = 1; i < n1.size(); i++)
        {
            n1[i - 1].next = &n1[i];
        }
    }
    for(vector<ListNode>& n2:nn2)
    {
        for(int i = 1; i < n2.size(); i++)
        {
            n2[i - 1].next = &n2[i];
        }
    }
    for(int i = 0 ; i < nn1.size(); i ++)
    {
        auto n1 = nn1[i],
             n2 = nn2[i];
        verify_sum(&n1[0], &n2[0], Solution().addTwoNumbers(&n1[0], &n2[0]));
    }
    return 0;
}