#include <iostream>
#include <queue>
using namespace std;
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
typedef TreeNode* tn;
TreeNode* bstFromQ(int lmin, int rmax, queue<TreeNode*>& bstq) {
    if(bstq.empty()) return nullptr;
    TreeNode* top = bstq.front();
    if(top->val > lmin && top->val < rmax)
    {// Process
        // First remove the item
        bstq.pop();
        top->left = bstFromQ(lmin, top->val, bstq);
        top->right = bstFromQ(top->val, rmax, bstq);
        return top;
    }
    return nullptr;
}
TreeNode* bstFromPreorder(vector<int>& preorder) {
    queue<TreeNode*> bstq;
    // Generate Queue
    for(auto i : preorder)
    {
        bstq.push(new TreeNode(i));
    }
    //TreeNode* root = bstq.top();bstq.pop();
    return bstFromQ(INT32_MIN, INT32_MAX, bstq);
    //return root;
}
void print_bfs(TreeNode* root){
    if(root == nullptr) return;
    queue<TreeNode*> q;
    q.push(root);
    while(!q.empty()) {
        TreeNode* n = q.front();q.pop();
        cout << n->val << " ";
        if(n->left) q.push(n->left);
        if(n->right) q.push(n->right);
    }
}
int main()
{
    queue<TreeNode*> qqq;
    vector<vector<int>> v = {
        {8,5,1,7,10,12},
        {1,2,3,4,5,6,7},
        {7,6,5,4,3,2,1},
        {8,5,10,9,12,11,18,16,14,17,19}
    };
    for (auto i : v)
    {
        //cout << "B:";
        TreeNode* root = bstFromPreorder(i);
        std::copy(i.begin(), i.end(), ostream_iterator<int>(cout, " "));
        cout << ":";
        print_bfs(root);
        cout << endl;
    }
    return 0;
}