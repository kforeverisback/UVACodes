#include <iostream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <stack>
#include <numeric>
#include <unordered_map>
#define XX ? "True":"False"
using namespace std;
string stringShift(string s, vector<vector<int>>& shiftarr) {
    if(s.size() < 2 || shiftarr.size() == 0 ) return s;
    // We won't shift the string for each op
    // We'll calculate first then shift the string once and for all!!
    char output[128] = {0};
    int shift = 0;
    for(vector<int> & i : shiftarr) {
        int dir = i[0], amount = i[1];
        if (dir == 0) { // Left shift
            shift -= amount;
        } else { // Right shift
            shift += amount;
        }
    }
    if(shift == 0) return s;
    // Very very important
    shift = shift % (int)s.size();
    if(shift < 0) shift += s.size();
    /**
     * @brief Instead of using a for loop
     * We can use this awesome technique
     * s = s + s;
     * return s.substr(shift, s.size());
     * :D
     */
    for(int i = 0 ; i < s.size(); i++, shift++)
    {
        // Reset shift if crosses the border
        if(shift >= s.size()) shift = 0;
        output[shift] = s[i];
    }
    return string(output);
}
int main()
{
    vector<pair<string,vector<vector<int>>>> v = {
        {"abc", {{0,1},{1,2}}},
        {"abcdef", {{0,1},{1,2}, {1,6},{0,8}}},
        {"abcdef", {}},
        {"a", {{0,1},{1,2}}},
        {"aaa", {{0,1},{1,2}}},
    };
    for (auto i : v)
    {
        //cout << "B:";
        cout << i.first << ":";
        cout << stringShift(i.first, i.second) << endl;
    }
    return 0;
}