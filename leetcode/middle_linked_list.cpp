#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
using namespace std;
#define XX ? "True":"False"
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};
ListNode* middleNodeHelper(ListNode* head, int &sz) {
    if(head == nullptr) return nullptr;
    int cur = sz++;
    ListNode* n = middleNodeHelper(head->next, sz);
    if(n) return n;
    if(sz / 2 == cur) return head;
    return nullptr;
}
ListNode* middleNode(ListNode* head) {
    int sz = 0;
    return middleNodeHelper(head, sz);
}
int main()
{
    #define NULL_NODE INT32_MIN
    // INT32_MIN means NULL node
    vector<ListNode> nodes = {1,2};
    for(int i = 0 ; i < nodes.size() - 1; i++)
    {
        nodes[i].next = &nodes[i+1];
    }
    cout << "Val:" << middleNode(&nodes[0])->val << endl;
    
    return 0;
}