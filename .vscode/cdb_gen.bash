#!/bin/bash

[[ $# -lt 4 ]] && >&2 echo "Missing args." && exit 1
file_name=$1
file_dir=$2
cmd=$3
out=$4
append_to_cdb=$5 # boolean, means we don't delete old files lists
[[ ! -f ${out} ]] && echo "[]" > ${out}
#cdb=`echo ${cdb} | sed "s%-FILE-%${file_name}%g"  | sed "s#-FILEDIR-#${file_dir}#g"  | sed "s?-CMD-?${cmd}?g" | jq -r .`
cdb=`jq -rn "{directory:\"${file_dir}\", command:\"${cmd}\", file:\"${file_name}\"}"`
if [[ ! -z $append_to_cdb ]] && [[ ! -z $(cat $out) ]];then
    jq ". | del(.[] | select(.file == \"${file_name}\" and .directory == \"${file_dir}\")) | .+=[${cdb}]" < ${out} > /var/tmp/jqtmp
    mv /var/tmp/jqtmp ${out}
else
    jq -rn ". | [${cdb}]" > ${out}
fi

#echo ${cdb} | grep -Eo '"[^"]*" *(: *([0-9]*|"[^"]*")[^{}\["]*|,)?|[^"\]\[\}\{]*|\{|\},?|\[|\],?|[0-9 ]*,?' | awk '{if ($0 ~ /^[}\]]/ ) offset-=4; printf "%*c%s\n", offset, " ", $0; if ($0 ~ /^[{\[]/) offset+=4}' > ${out}
