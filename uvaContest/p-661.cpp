//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>


typedef long long			ll;
typedef unsigned int		ui;
typedef unsigned long long	ull;
typedef unsigned short		us;

//C++
#include <iostream>
#include <bitset>
using namespace std;

//C
#define si(_V_)	scanf("%d", &_V_)
#define sii(_V_1,_V_2)	scanf("%d %d", &_V_1, &_V_2)
#define siii(_V_1,_V_2,_V_3)	scanf("%d %d %d", &_V_1,&_V_2,&_V_3)
#define sc(_V_)	scanf("%c", &_V_)
#define ss(_V_)	scanf("%s", _V_)
#define rep(i,n) for(int i=0, _##i=(n); i<_##i; ++i)
#define dwn(i,n) for(int i=(n); --i>=0; )
#define forinf	for(;;)
#define whileinf while(true)
#define SafeDelete(_V_) if(nullptr != _V_) {delete _V_; _V_ = nullptr;}

#define char2Int(c) (c-'0')
#define maX(a,b)                     ( (a) > (b) ? (a) : (b))
#define miN(a,b)                     ( (a) < (b) ? (a) : (b))
#define checkbit(n,b)                ( (n >> b) & 1)
#define getbit(x,i) (x&(1<<i))  //select the bit of position i of x
#define getlowbit(x) ((x)&((x)^((x)-1))) //get the lowest bit of x
#define setbit(number,pos) ((number) |= 1 << (pos))
#define clearbit(number,pos) ((number) &= ~(1 << (pos)))
#define togglebit(number,pos)	((number) ^= 1 << (pos));
#define hBit(msb,n) asm("bsrl %1,%0" : "=r"(msb) : "r"(n)) //get the highest bit of x, maybe the fastest
#define abS(x) (x<0?(-x):x) // big bug here if "-x" is not surrounded by "()"


#define MAX_DEV_NUM 25

int main(int argc, char** argv)
{
	freopen("p-661.txt", "r", stdin);
	int N, M, C;
	int seq = 0;

	int Ns[MAX_DEV_NUM + 1] = { 0 };
	std::bitset<MAX_DEV_NUM + 1> ds(0x00);
	forinf
	{
		siii(N, M, C);
		if (!(N | M | C))
		{
			return EXIT_SUCCESS;
		}

		int max = 0, amp = 0;
		//printf("Sequence %d: N,M,C: %d, %d, %d\n", ++seq, N, M, C);
		printf("Sequence %d\n", ++seq);
		rep(i, N)
		{
			si(Ns[i]);
		}

		ds.reset();
		for (; M > 0; M--)
		{
			int c;
			si(c);
			c--;
			amp += !ds[c] * Ns[c] - ds[c] * Ns[c];
			ds.flip(c);
			if (amp > C) { printf("Fuse was blown.\n\n"); M--; goto ignore; }
			if (amp > max) max = amp;
		}

		printf("Fuse was not blown.\nMaximal power consumption was %d amperes.\n\n", max);
		continue;
	ignore:
		for (; M > 0; M--)
		{
			scanf("%*d");
		}
	}
	return EXIT_SUCCESS;
}