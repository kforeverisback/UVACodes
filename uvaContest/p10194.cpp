#if 1
//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef long long           ll;
typedef unsigned int        ui;
typedef unsigned long long  ull;
typedef unsigned short      us;
typedef unsigned char       uc;

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>
#include <map>
#include <bitset>
#include <set>
#include <fstream>
#include <stack>
#include <unordered_map>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <functional> //plus, minus, multiplies, divides, modulus, equal_to,not_equal_to, greater*,less*
#include <utility> //swap, make_pair, move, 

using namespace std;
typedef std::pair<int, int> pii;
typedef std::vector<int>    vi;
typedef std::vector<pii>    vpii;

//#define PI 3.1415926535897932384626
#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
#define MEMSET_INF 127 // about 2B
#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//C
#define si(_V_) scanf("%d", &_V_)
#define sii(_V_1,_V_2)  scanf("%d %d", &_V_1, &_V_2)
#define siii(_V_1,_V_2,_V_3)    scanf("%d %d %d", &_V_1,&_V_2,&_V_3)
#define sc(_V_) scanf("%c", &_V_)
#define ss(_V_) scanf("%s", _V_)
#define rep(i,n) for(int i=0, _##i=(n); i<_##i; ++i)
#define dwn(i,n) for(int i=(n); --i>0; )
#define rep_iv(i,iv,n) for(int i=iv, _##i=(n); i<_##i; ++i)
#define dwn_lv(i,lv,n) for(int i=(n); --i>=lv; )
#define forinf  for(;;)
#define whileinf while(true)
#define SafeDelete(_V_) if(nullptr != _V_) {delete _V_; _V_ = nullptr;}

#define char2Int(c) (c-'0')
#define maX(a,b)                     ( (a) > (b) ? (a) : (b))
#define miN(a,b)                     ( (a) < (b) ? (a) : (b))
#define checkbit(n,b)                ( (n >> b) & 1)
#define getbit(x,i) (x&(1<<i))  //select the bit of position i of x
#define getlowbit(x) ((x)&((x)^((x)-1))) //get the lowest bit of x
#define setbit(number,x) ((number) |= 1 << (x))
#define clearbit(number,x) ((number) &= ~(1 << (x)))
#define togglebit(number,x) ((number) ^= 1 << (x));
#define hBit(msb,n) asm("bsrl %1,%0" : "=r"(msb) : "r"(n)) //get the highest bit of x, maybe the fastest
#define abS(x) (x<0?(-x):x) // big bug here if "-x" is not surrounded by "()"

#define str_tolower(_str_) std::transform(_str_.begin(), _str_.end(), _str_.begin(), ::toupper)

//C++
#define mp std::make_pair
#define lastEle(vec) vec[vec.size()-1]
#define pb push_back
#define sz(a) (int)(a.size())


typedef struct _TeamData
{
	string name, low_name;
	int total_points_earned ,
		games_played ,
		wins ,
		loses ,
		ties ,
		goals_scored ,
		goals_against ;

	_TeamData() : _TeamData("") {}
	_TeamData(string n) :name(n), low_name(n), total_points_earned(0), games_played(0), wins(0), loses(0), ties(0), goals_against(0), goals_scored(0)
	{
		str_tolower(low_name);
	}
	string toString()
	{
		char print[512] = { 0 };
		sprintf(print, "%s %dp, %dg (%d-%d-%d), %dgd (%d-%d)", name.c_str(),total_points_earned,games_played, wins, ties, loses, goals_scored - goals_against, goals_scored, goals_against);
		return string(print);
	}

} TeamData;

typedef std::map<string, int> TeamMap;

struct less_than_key
{
	inline bool operator() (const TeamData& td1, const TeamData& td2)
	{
		return (td1.total_points_earned > td2.total_points_earned) ? true :
			(td1.total_points_earned < td2.total_points_earned) ? false :
			(td1.wins > td2.wins) ? true :
			(td1.wins < td2.wins) ? false :
			((td1.goals_scored - td1.goals_against) >(td2.goals_scored - td2.goals_against)) ? true :
			((td1.goals_scored - td1.goals_against) < (td2.goals_scored - td2.goals_against)) ? false :
			td1.goals_scored > td2.goals_scored ? true :
			td1.goals_scored < td2.goals_scored ? false :
			td1.games_played < td2.games_played ? true :
			td1.games_played > td2.games_played ? false :
			td1.low_name < td2.low_name;
	}
};

int main(int argc, char** argv)
{
	//ONLINE_JUDGE is added in the Project settings :)
#ifndef ONLINE_JUDGE
	freopen("p10194.txt", "r", stdin);
	freopen("output.txt", "w+", stdout);
#endif
	int TC;
	cin >> TC;
	cin.ignore();
	for (; TC > 0;TC--)
	{
		char tour_name[101] = { 0 };
		cin.getline(tour_name, 101);
		cout << tour_name << endl;
		std::vector<TeamData> teams;
		int team_cnt;
		TeamMap team_maps;
		{
			cin >> team_cnt; cin.ignore();
			rep(j, team_cnt)
			{
				string tn;
				std::getline(cin, tn);
				teams.push_back(TeamData(tn));
				team_maps[tn] = j;
			}
		}

		int match_cnt;
		cin >> match_cnt; cin.ignore();
		rep(j, match_cnt)
		{
			char mt2[512] = { 0 };
			cin.getline(mt2, 512);
			char* ptr1 = mt2;
			char* ptr2 = strchr(mt2, '@');
			ptr2[0] = '\0';
			ptr2++;
			//1st team info
			{
				char *goals = strchr(ptr1, '#');
				goals[0] = '\0';
				goals++;
				TeamData &td1 = teams[team_maps[ptr1]];
				td1.games_played++;
				int td1Goal = atoi(goals);
				td1.goals_scored += td1Goal;

				//2nd team info
				goals = strchr(ptr2, '#');
				ptr2 = goals + 1;
				goals[0] = '\0';
				goals--;
				TeamData &td2 = teams[team_maps[ptr2]];
				td2.games_played++;
				int td2Goal = atoi(goals);
				td2.goals_scored += td2Goal;

				td2.goals_against += td1Goal;
				td1.goals_against += td2Goal;

				//
				if (td1Goal> td2Goal)
				{
					td1.total_points_earned += 3;
					td1.wins++;

					td2.loses++;
				}
				else if (td1Goal < td2Goal)
				{
					td2.total_points_earned += 3;
					td2.wins++;

					td1.loses++;
				}
				else
				{
					td1.ties++;
					td2.ties++;
					td1.total_points_earned++;
					td2.total_points_earned++;
				}

				//cout << "T1:" << td1.toString() << endl << "T2:" << td2.toString() << endl;
			}
		}

		std::sort(teams.begin(), teams.end(), less_than_key());
		for (int i = 0; i < sz(teams); i++ )
		{
			cout << i + 1 << ") " << teams[i].toString() << endl;
		}

		if (TC != 1)
			cout << endl ;
	}
	return 0;
}
#else
/*
Football (aka Soccer)
Jose Ricardo Bustos Molina
*/

#include <iostream>
#include <map>
#include <vector>
#include <sstream>
#include <algorithm>
#include <stdlib.h>

using namespace std;

class equipo{

public:

	int points;
	int games;
	int wins;
	int ties;
	int losses;
	int scored;
	int against;
	string name;
	string nameM;

	equipo(){
		points = 0;
		games = 0;
		wins = 0;
		ties = 0;
		losses = 0;
		scored = 0;
		against = 0;
	}

	void setname(string n){
		name = n;
		nameM = n;
		transform(nameM.begin(), nameM.end(), nameM.begin(), ::toupper);
	}

	void add(int s, int a){
		games++;
		scored += s;
		against += a;
		if (s>a){
			wins++;
			points += 3;
		}
		else if (s == a){
			ties++;
			points += 1;
		}
		else{
			losses++;
		}
	}

};

bool ordenar(const equipo& uno, const equipo& otro){
	if (uno.points > otro.points) return true;
	else if (uno.points < otro.points) return false;
	if (uno.wins > otro.wins) return true;
	else if (uno.wins < otro.wins) return false;
	if (uno.scored - uno.against > otro.scored - otro.against) return true;
	else if (uno.scored - uno.against < otro.scored - otro.against) return false;
	if (uno.scored > otro.scored) return true;
	else if (uno.scored < otro.scored) return false;
	if (uno.games < otro.games) return true;
	else if (uno.games > otro.games) return false;
	return uno.nameM < otro.nameM;
}

int main(){
#ifndef ONLINE_JUDGE
	freopen("p10194.txt", "r", stdin);
	freopen("output2.txt", "w+", stdout);
#endif
	int T;
	cin >> T;
	cin.get();
	for (int t = 0; t<T; ++t){
		string torneo;
		getline(cin, torneo);
		if (t>0) cout << endl;
		cout << torneo << endl;
		int E;
		cin >> E;
		cin.get();
		map<string, equipo> m;
		for (int e = 0; e<E; ++e){
			string eq;
			getline(cin, eq);
			m[eq] = equipo();
			m[eq].setname(eq);
		}
		int J;
		cin >> J;
		cin.get();
		for (int j = 0; j<J; ++j){
			string juego;
			getline(cin, juego);
			int p1 = juego.find('#');
			int p2 = juego.find('@');
			int p3 = juego.find('#', p1 + 1);
			string eq1 = juego.substr(0, p1);
			int g1 = atoi((juego.substr(p1 + 1, p2 - p1 - 1)).data());
			int g2 = atoi(juego.substr(p2 + 1, p3 - p2 - 1).data());
			string eq2 = juego.substr(p3 + 1, juego.size() - 1 - p3);
			m[eq1].add(g1, g2);
			m[eq2].add(g2, g1);
		}
		vector<equipo> v;
		for (map<string, equipo>::iterator it = m.begin(); it != m.end(); ++it) v.push_back((*it).second);
		sort(v.begin(), v.end(), ordenar);
		for (int i = 0; i<v.size(); ++i)
			cout << i + 1 << ") " << v[i].name << " " << v[i].points << "p, " << v[i].games << "g (" << v[i].wins << "-" << v[i].ties << "-" << v[i].losses << "), " << v[i].scored - v[i].against << "gd (" << v[i].scored << "-" << v[i].against << ")" << endl;
	}
}

#endif