//Benchmark
#include "CPUBenchmark.h"
#include <iostream>
#include <vector>
using namespace std;
#if 1 //
//with push_back
int runtestnice(size_t N) {
	vector<int> bigarray;
	for (unsigned int k = 0; k<N; ++k)
		bigarray.push_back(k);
	int sum = 0;
	for (unsigned int k = 0; k<N; ++k)
		sum += bigarray[k];
	return sum;
}
//with push_back and reserve call
int runtestnicewreserve(size_t N) {
	vector<int> bigarray;
	bigarray.reserve(N);
	for (unsigned int k = 0; k<N; ++k)
		bigarray.push_back(k);
	int sum = 0;
	for (unsigned int k = 0; k<N; ++k)
		sum += bigarray[k];
	return sum;
}

//init vector first
int runtestsafe(size_t N) {
	vector<int> bigarray(N);
	for (unsigned int k = 0; k<N; ++k)
		bigarray[k] = k;
	int sum = 0;
	for (unsigned int k = 0; k<N; ++k)
		sum += bigarray[k];
	return sum;
}

//init vector first
int runtestunsafe(size_t N) {
	vector<int> bigarray;
	bigarray.reserve(N);
	for (unsigned int k = 0; k<N; ++k)
		bigarray[k] = k;// unsafe
	int sum = 0;
	for (unsigned int k = 0; k<N; ++k)
		sum += bigarray[k];
	return sum;
}

//pure C way no bad-ass vector
int runtestclassic(size_t N) {
	int * bigarray = new int[N];
	for (unsigned int k = 0; k<N; ++k)
		bigarray[k] = k;
	int sum = 0;
	for (unsigned int k = 0; k<N; ++k)
		sum += bigarray[k];
	delete[] bigarray;
	return sum;
}

//without alloc
int runtestnoalloc(size_t N, int * bigarray) {
	for (unsigned int k = 0; k<N; ++k)
		bigarray[k] = k;// unsafe
	int sum = 0;
	for (unsigned int k = 0; k<N; ++k)
		sum += bigarray[k];
	return sum;
}

//with custom iterators
template <typename T>
struct iota_iterator : std::iterator<std::forward_iterator_tag, T> {
	iota_iterator(T value = T()) : value(value) { }
	iota_iterator& operator ++() {
		++value;
		return *this;
	}
	iota_iterator operator ++(int) {
		iota_iterator copy = *this;
		++*this;
		return copy;
	}
	T const& operator *() const {
		return value;
	}
	T const* operator ->() const {
		return &value;
	}
	friend bool operator ==(iota_iterator const& lhs, iota_iterator const& rhs) {
		return lhs.value == rhs.value;
	}
	friend bool operator !=(iota_iterator const& lhs, iota_iterator const& rhs) {
		return !(lhs == rhs);
	}
private:
	T value;
};
int runtestgenerator(size_t N) {
	// Extra parentheses to prevent most vexing parse.
	vector<int> bigarray((iota_iterator<int>()), iota_iterator<int>(N));
	int sum = 0;
	for (unsigned int k = 0; k<N; ++k)
		sum += bigarray[k];
	return sum;
}
int main_vector_bench() {
	CPUCycleBenchmark time;
	//const size_t N = 1000 * 1000 * 100;
	const size_t N = 1000 * 1000;
	time.start();
	cout.precision(3);
	cout << " report speed in CPU cycles per integer" << endl;
	cout << endl << "ignore this:" << runtestnice(N) << endl;
	cout << "with push_back:" << (time.stop()*1.0 / N) << endl;
	time.start();
	cout << endl << "ignore this:" << runtestnicewreserve(N) << endl;
	cout << "with push_back and reserve:" << (time.stop()*1.0 / N) << endl;
	time.start();
	cout << endl << "ignore this:" << runtestsafe(N) << endl;
	cout << "init first:" << (time.stop()*1.0 / N) << endl;
	time.start();
	/*cout << endl << "ignore this:" << runtestunsafe(N) << endl;
	cout << "reserve first:" << (time.stop()*1.0 / N) << endl;
	time.start();*/
	cout << endl << "ignore this:" << runtestclassic(N) << endl;
	cout << "C++ new:" << (time.stop()*1.0 / N) << endl;
	int * bigarray = new int[N];
	time.start();
	cout << endl << "ignore this:" << runtestnoalloc(N, bigarray) << endl;
	cout << "without alloc:" << (time.stop()*1.0 / N) << endl;
	time.start();
	cout << endl << "ignore this:" << runtestgenerator(N) << endl;
	cout << "generator iterators:" << (time.stop()*1.0 / N) << endl;
	return 0;
}
#endif