//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef long long			ll;
typedef unsigned int		ui;
typedef unsigned long long	ull;
typedef unsigned short		us;
typedef unsigned char		uc;

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>
#include <map>
#include <bitset>
#include <set>
#include <fstream>
#include <stack>
#include <queue>
#include <unordered_map>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <functional> //plus, minus, multiplies, divides, modulus, equal_to,not_equal_to, greater*,less*
#include <utility> //swap, make_pair, move, 

using namespace std;
typedef std::pair<int, int>	pii;
typedef std::vector<int>	vi;
typedef std::vector<pii>	vpii;

//#define PI 3.1415926535897932384626
#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
#define MEMSET_INF 127 // about 2B
#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//C
#define si(_V_)	scanf("%d", &_V_)
#define sii(_V_1,_V_2)	scanf("%d %d", &_V_1, &_V_2)
#define siii(_V_1,_V_2,_V_3)	scanf("%d %d %d", &_V_1,&_V_2,&_V_3)
#define sc(_V_)	scanf("%c", &_V_)
#define ss(_V_)	scanf("%s\n", _V_)

#define MAX_LEN 50
#define PRIORITY_INDX MAX_LEN + 4
#define MAX_ITEMS 100

int N, M, TC;

int calc_inversion(const char* item)
{
	//printf("==>%s\n", item);
	const char *end = &item[N - 1];
	int invCount = 0;
	for (int i = 0; i < N - 1; i++)
	{
		char c = item[i];
		switch (c)
		{ 
		case'A':
			continue;
		case 'C':
			for (int j = i + 1; j < N; j++)
			{
				invCount += (item[j] == 'A');
			}
			break;
		case 'G':
			for (int j = i+1; j < N; j++)
			{
				invCount += (item[j] == 'A' || item[j] == 'C');
			}
			break;
		case 'T':
			for (int j = i+1; j < N; j++)
			{
				invCount += (item[j] == 'A' || item[j] == 'C' || item[j] == 'G');
			}
			break;
		default:
			break;
		}
	}
	return invCount;
}

int main(int argc, char** argv)
{
	//ONLINE_JUDGE is added in the Project settings :)
#ifndef ONLINE_JUDGE
	//freopen("p612.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
#endif

	//UVA FUCKING PSYCHOPATH!
	//IF IT OUTPUTS AN EXTRA "NEW LINE"
	//IT IS A FUCKING WRONG ANSWER!
	//WTF!

	scanf("%d\n", &TC);
	char inputs[MAX_ITEMS][PRIORITY_INDX + 1] = { { 0 } };
	for (; TC > 0; TC--)
	{
		std::map<int, vector<const char*>> maps;
		sii(N, M);
		for (int i = 0; i < M; i++)
		{
			ss(inputs[i]);
			//inputs[i][PRIORITY_INDX] = calc_inversion(inputs[i]);
			int key = calc_inversion(inputs[i]);
			maps[key].push_back(inputs[i]);
		}

		for (std::map<int, vector<const char*>>::const_iterator ci = maps.begin(); ci != maps.end(); ci++)
		{
			//cout << "Key: " << ci->first << ", Val:" << ci->second << endl;
			for (const char* c : ci->second)
			{
				cout << c << endl;
			}
		}

		if (TC != 1)
			cout << endl;
	}
	//if (TC != 1)
	//cout << endl;
	return 0;
}