//C
#include <iostream>
//C
#define sii(_V_1,_V_2)	scanf("%d %d", &_V_1, &_V_2)

int L[100000+2], R[100000+2];
void repopulate_sides(int S)
{
	L[1] = -1;
	R[S] = -1;
	for (int i = 1; i < S; i++)
	{
		L[i + 1] = i;
		R[i] = i + 1;
	}
}
int main(int argc, char** argv)
{
    //freopen("E:/dev/UVACodes/uvaContest/sample_input/p-12356-ArmyBuddies.txt", "r", stdin);
    //freopen("/Users/mekram/Documents/mydev/UVACodes/uvaContest/sample_input/p-12356-ArmyBuddies.txt", "r", stdin);
    
    while(true)
    {
        int S,B;
        sii(S,B);
        if (S == 0 && B == 0)
            break;
        repopulate_sides(S);
        for(; B > 0 ; B--)
        {
            int l, r;
            sii(l, r);
            int rr = R[r], ll = L[l];
            if (r != S)
                L[rr] = ll;
            if (l != 1)
                R[ll] = rr;

            ll = L[l], rr = R[r];
            if (ll == -1)
                printf("* ");
            else
                printf("%d ",ll);
            if (rr == -1)
                printf("*\n");
            else
                printf("%d\n", rr);
        }
        printf("-\n");
    }
    
    return 0;
}
