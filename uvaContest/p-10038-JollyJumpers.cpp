//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef long long           ll;
typedef unsigned int        ui;
typedef unsigned long long  ull;
typedef unsigned short      us;
typedef unsigned char       uc;

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>
#include <map>
#include <bitset>
#include <set>
#include <fstream>
#include <stack>
#include <unordered_map>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <functional> //plus, minus, multiplies, divides, modulus, equal_to,not_equal_to, greater*,less*
#include <utility> //swap, make_pair, move,

using namespace std;
typedef std::pair<int, int> pii;
typedef std::vector<int>    vi;
typedef std::vector<pii>    vpii;

//int diff_tracker[3000+1]={0};
#define JOLLY_STR "Jolly\n"
#define NO_JOLLY_STR "Not jolly\n"
int main(int argc, char** argv)
{
    //freopen("/Users/mekram/Documents/mydev/UVACodes/uvaContest/sample_input/p-10038-JollyJumpers.txt", "r", stdin);
    //freopen("/Users/mekram/Documents/mydev/UVACodes/uvaContest/sample_input/newInput", "r", stdin);

    string line;
    while( getline( cin, line ) )
    {
        int cur_number, number_count, prev_number = 0, sum = 0;
        bool is_error = false;
        istringstream iss( line );
        iss >> number_count >> prev_number;
        if(number_count == 1)
        {
            printf(JOLLY_STR);
            continue;
        }
        int target_sum = (number_count * (number_count - 1)) / 2;
        while( iss >> cur_number && !is_error)
        {
            int diff = abs(cur_number - prev_number);
            is_error = !diff ||  // Check whether we have similar number, diff can't be zero, coz range is 1 to n-1
                        !(sum <= target_sum); // If the sum is greater than the target, ERROR
            sum += diff;
            prev_number = cur_number;
        }
        //printf(line.c_str());
        if(target_sum == sum && !is_error)
            printf(JOLLY_STR);
        else
            printf(NO_JOLLY_STR);

    }
    return 0;
}
