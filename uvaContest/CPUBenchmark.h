#include <chrono>

typedef std::chrono::high_resolution_clock clock_;
typedef std::chrono::duration<double, std::ratio<1> > second_;
typedef std::chrono::duration<double, std::ratio<1000> > milliSecond_;
typedef std::chrono::duration<double, std::ratio<1000000> > nanoSecond_;


class CPUBenchMark{
public:
	CPUBenchMark() :
		_currTimePoint(clock_::now()){
	}
	std::chrono::time_point<clock_> _currTimePoint;
	double lastBenchmark;

	_inline void restart() {
		_currTimePoint = clock_::now();
	}
	
	_inline double stop() {
		lastBenchmark = std::chrono::duration_cast<milliSecond_>(clock_::now() - _currTimePoint).count() * 1000;
		return lastBenchmark;
	}

	_inline double getLastResult() const {
		return lastBenchmark;
	}

	_inline double getLastResultInMicro() const {
		return lastBenchmark * 1000;
	}
};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Use this
#include <Windows.h>
class CPUCycleBenchmark
{
	LARGE_INTEGER start_time_;
	LONGLONG _cycles;
public:
	CPUCycleBenchmark() { QueryPerformanceCounter(&start_time_); }
	_inline void   start() { QueryPerformanceCounter(&start_time_); }
	_inline long long stop() 
	{ 
		LARGE_INTEGER end_time, frequency;
		QueryPerformanceCounter(&end_time);
		QueryPerformanceFrequency(&frequency);
		_cycles = end_time.QuadPart - start_time_.QuadPart;
		return _cycles;
	}

	_inline double getElapsedTime() const
	{
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		return double(_cycles) / frequency.QuadPart;
	}
};