#include <iostream>
#include <vector>
#include <map>
#include <cstdio>
#include <cmath>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <utility>

using namespace std;
typedef long long			ll;
typedef std::pair<int, int>	pii;
typedef std::vector<int>	vi;
typedef std::vector<pii>	vpii;

#define INF 1e9

#define pb push_back
#define sz(a) (int)(a.size())

#define si(_V_)	scanf("%d", &_V_)
#define sii(_V_1,_V_2)	scanf("%d %d", &_V_1, &_V_2)
#define siii(_V_1,_V_2,_V_3)	scanf("%d %d %d", &_V_1,&_V_2,&_V_3)
#define sc(_V_)	scanf("%c", &_V_)
#define ss(_V_)	scanf("%s", _V_)
#define rep(i,n) for(int i=0, _##i=(n); i<_##i; ++i)
#define dwn(i,n) for(int i=(n); --i>=0; )
#define mp std::make_pair
#define lastEle(vec) vec[vec.size()-1] 
#define SafeDelete(_V_) if(nullptr != _V_) {delete _V_; _V_ = nullptr;}

#pragma region DEBUG_HELPERS
//DEBUG

#if 0
#define dbg(...) do { fprintf(stderr, __VA_ARGS__); fflush(stderr); } while (0)

template <typename T1, typename T2>
inline std::ostream& operator << (std::ostream& os, const std::pair<T1, T2>& p)
{
	return os << "(" << p.first << ", " << p.second << ")";
}


template<typename T>
inline std::ostream &operator << (std::ostream & os, const std::vector<T>& v)
{
	bool first = true;
	os << "[";
	for (unsigned int i = 0; i < v.size(); i++)
	{
		if (!first)
			os << ", ";
		os << v[i];
		first = false;
	}
	return os << "]";
}


template<typename T>
inline std::ostream &operator << (std::ostream & os, const std::set<T>& v)
{
	bool first = true;
	os << "[";
	for (typename std::set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
	{
		if (!first)
			os << ", ";
		os << *ii;
		first = false;
	}
	return os << "]";
}


template<typename T1, typename T2>
inline std::ostream &operator << (std::ostream & os, const std::map<T1, T2>& v)
{
	bool first = true;
	os << "[";
	for (typename std::map<T1, T2>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
	{
		if (!first)
			os << ", ";
		os << *ii;
		first = false;
	}
	return os << "]";
}

#define LOG(_X_) cerr << _X_ <<endl
#else
#define LOG(_X_)
#define dbg(...)
#endif
//DEBUG END
#pragma endregion



//////////////////CODE
typedef struct _data
{
	int B; int D; int CD; vi Ld, Ud, Cd;

	_data()
	{
		Ld.pb(-1);
		Ud.pb(-1);
	}
} data;

void calc_Cds(data& d)
{
	d.Cd = std::move(vi(d.D + 1));
	lastEle(d.Cd) = d.CD;
	int totalCL = 0;
	for (int i = d.D; i > 1; i--)
	{
		int val = d.Cd.at(i) * (d.Ud.at(i) - d.Ld.at(i) + 1);
		d.Cd.at(i - 1) = val;
		totalCL += val * d.Ld.at(i - 1);
	}
	totalCL += lastEle(d.Cd)*lastEle(d.Ld);

	d.Cd.at(0) = d.B - totalCL;

	LOG("Cds: " << d.Cd);
}

typedef map<std::string, data> datamap;
//#include "CPUBenchmark.h"
int main(int argc, char** argv)
{
	freopen("p-394.txt", "r", stdin);
	//CPUCycleBenchmark cb;
	//freopen("p-394-out.txt", "w+", stdout);
	int N, R;
	sii(N, R);

	//INPUT
	datamap dm;
	rep(i,N)
	{
		char s[11] = {0};
		ss(s);
		dm[s] = std::move(data());
		data &d = dm[s];
		siii(d.B, d.CD, d.D);

		rep(i, d.D)
		{
			int v;
			si(v);
			d.Ld.pb(v);
			si(v);
			d.Ud.pb(v);
		}

		LOG("Ld:" << d.Ld);
		LOG("Ud:" << d.Ud);

		calc_Cds(d);
	}

	rep(i, R)
	{
		char s[11] = { 0 };
		ss(s);
		printf("%s[", s);
		const data &d = dm[s];
		int baseCalc = d.Cd[0];
		
		int j = 0;
		while (true)
		{
			int val;
			si(val);
			printf("%d", val);
			baseCalc += val* d.Cd[j + 1];
			if (++j < d.D)
			{
				printf(", ");
			}
			else
			{ 
				printf("] = %d\n", baseCalc);
				break;
			}
		}

		/*rep(j, d.D)
		{
			int val;
			si(val);
			printf("%d", val);
			baseCalc += val* d.Cd[j + 1];
		}*/
		LOG(baseCalc);
	}

	//PROBLEM
	//auto val = cb.stop();
	//cout << val << endl;
	return 0;
}