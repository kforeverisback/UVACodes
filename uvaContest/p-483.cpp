#include <iostream>
#include <vector>
#include <map>
#include <cstdio>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <set>
#include <fstream>
#include <stack>
#include <unordered_map>
#include <algorithm>
#include <utility>

#include <cstdlib>
#include <cstring>
using namespace std;

int main(int argc, char** argv)
{
	freopen("p-483.txt", "r", stdin);
	//freopen("p-483-out.txt", "w", stdout);

	stringstream ss;
	while (true)
	{
		string inp;
		if (!getline(cin, inp))
			break;

		if (inp[0] == '\0')
		{
			ss << "\n";
			continue;
		}
		const char* origPtr = inp.data();
		const char *spacePtr = strchr(origPtr, ' ');

		while (nullptr != spacePtr)
		{
			int len = (spacePtr - origPtr);
			const char* itr = spacePtr;
			while(len--)
			{
				itr--;
				ss << itr[0];
				//printf("%c", itr[0]);
			}
			ss << " ";
			//printf(" ");
			origPtr = spacePtr + 1;
			spacePtr = strchr(origPtr, ' ');
		}
		
		int len = strlen(origPtr);
		const char* itr = &origPtr[len - 1];
		while (len--)
		{
			ss << itr[0];
			//printf("%c", itr[0]);
			itr--;
		}
		
#if 0
		char *data = strtok(&inp.front(), " ");
		while (nullptr != data)
		{
			string str = data;
			if (sz(str) == 1) cout << str;
			else
				for (string::reverse_iterator s = str.rbegin();
					s != str.rend(); 
					++s) 
				{
					cout <<*s;
				}
			cout << " ";
			data = strtok(nullptr, " ");
		}
#endif
		ss << "\n";
		//printf("\n");
	}
	printf("%s", ss.str().data());
	return 0;
}