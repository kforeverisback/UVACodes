//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef long long			ll;
typedef unsigned int		ui;
typedef unsigned long long	ull;
typedef unsigned short		us;
typedef unsigned char		uc;

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>
#include <map>
#include <bitset>
#include <set>
#include <fstream>
#include <stack>
#include <unordered_map>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <functional> //plus, minus, multiplies, divides, modulus, equal_to,not_equal_to, greater*,less*
#include <utility> //swap, make_pair, move, 

using namespace std;


//C
#define si(_V_)	scanf("%d\n", &_V_)
#define sii(_V_1,_V_2)	scanf("%d %d", &_V_1, &_V_2)
#define siii(_V_1,_V_2,_V_3)	scanf("%d %d %d\n", &_V_1,&_V_2,&_V_3)
#define sc(_V_)	scanf("%c", &_V_)
#define ss(_V_)	scanf("%s\n", _V_)

int main(int argc, char** argv)
{
	//ONLINE_JUDGE is added in the Project settings :)
#ifndef ONLINE_JUDGE
	freopen("p11340.txt", "r", stdin);
#endif

	int TC = 0;
	cin >> TC;
	cin.ignore();
	for (; TC > 0; TC--)
	{
		//make sure the range is the full range of ASCII(usually upto 512) not the printable ones which is only upto 128
		//That is why the max array size is 512
		int values[512] = { 0 };
		int valCount = 0, lineCount = 0;
		cin >> valCount;
		cin.ignore();
		{
			int v;
			char c;
			for (; valCount > 0; valCount--)
			{
				scanf("\n%c %d", &c, &v);
				values[c] = v;
			}
		}
		cin >> lineCount;
		cin.ignore();
		unsigned char c = 0;
		unsigned int centValue = 0;
		for (; lineCount > 0; lineCount--)
		{
			while (sc(c) > 0 && c != '\n')
			{
				if (c > 512)
					continue;
				centValue += values[c];
			}
		}
		printf("%.2lf$\n", ((double)centValue)/ 100.0);
	}

	return 0;
}