#if 1
//C++ HEADER
#include <cstdio>
#include <cstring>
int TEST_CASE;

#define d30 30
#define d31 31
#define d28 28

#define jan d31
#define feb d28
#define mar d31
#define apr d30
#define may d31
#define jun d30
#define jul d31
#define aug d31
#define sep d30
#define oct d31
#define nov d30
#define dec d31
#define DATE(Y,M,D) 10000*Y+100*M+D
#define DATE_CMP(D1,D2) D1>D2

int monthDays[] = { jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec };


int main_p10942()
{
	freopen("10942.txt", "r", stdin);
	scanf("%d", &TEST_CASE);
	int dates[4][3] = { { 3, 6, 9 }, { 6, 3, 9 }, { 3, 9, 6 }, { 9, 6, 3 } };
	int *num = dates[0];
	printf(">> %d", DATE(num[0], num[1], num[2]));
	num = dates[1];
	printf(">> %d", DATE(num[0], num[1], num[2]));
	num = dates[2];
	printf(">> %d", DATE(num[0], num[1], num[2]));
	num = dates[3];
	printf(">> %d", DATE(num[0], num[1], num[2]));
	while (TEST_CASE--){
		
	}

	return 0;
}
#else
#endif