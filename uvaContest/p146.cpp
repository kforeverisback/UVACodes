//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef long long           ll;
typedef unsigned int        ui;
typedef unsigned long long  ull;
typedef unsigned short      us;
typedef unsigned char       uc;

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>
#include <map>
#include <bitset>
#include <set>
#include <fstream>
#include <stack>
#include <unordered_map>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <functional> //plus, minus, multiplies, divides, modulus, equal_to,not_equal_to, greater*,less*
#include <utility> //swap, make_pair, move, 

using namespace std;
typedef std::pair<int, int> pii;
typedef std::vector<int>    vi;
typedef std::vector<pii>    vpii;

#define sz(a) (int)(a.size())


int main(int argc, char** argv)
{
    //ONLINE_JUDGE is added in the Project settings :)
#ifndef ONLINE_JUDGE
    freopen("p146.txt", "r", stdin);
#endif
    while (1)
    {
        string code_chars;
        std::getline(cin, code_chars);
        //cin.getline(code_chars, 50);

        if (code_chars[0] == '#')
            break;

        bool found_successor = false;
        for (int i = sz(code_chars) - 2; i >= 0; i--)
        {
            if (code_chars[i] < code_chars[i + 1])
            {
                char* max_char = &code_chars[i + 1];
                int cur_diff = *max_char - code_chars[i];
                for (int j = i + 2; j < sz(code_chars); j++)
                {
                    if (code_chars[i] < code_chars[j] && (cur_diff > (code_chars[j] - code_chars[i])))
                    {
                        max_char = &code_chars[j];
                    }
                }
                std::swap(code_chars[i], *max_char);
                //std::sort(&code_chars[i + 1], &code_chars[len - 1]);
                std::sort(code_chars.begin()+ i + 1, code_chars.end());
                found_successor = true;
                break;
            }
        }
        cout << (found_successor?code_chars:"No Successor") << endl;
    }
    return 0;
}