//C
#include <cstdlib>
#include <cstdio>

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>

#include <fstream>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <utility> //swap, make_pair, move, 

using namespace std;

#define MAX_LINE 4096
typedef std::vector <string> vs;
int main(int argc, char** argv)
{
#ifndef ONLINE_JUDGE
	freopen("p-482.txt", "r", stdin);
#endif
	int TC = 0;
	char line[MAX_LINE] = { 0 };
	//std::string line2;
	scanf("%d\n", &TC);
	vs lines(MAX_LINE);
	for (; TC > 0; TC--)
	{
		cin.getline(line, MAX_LINE);
		stringstream intStream(line);
		int xxx = 0, max = 0;
		while (!intStream.eof())
		{
			string val;
			intStream >> xxx;
			max = std::max(max, xxx);
			cin >> val;
			lines[xxx - 1] = val;
		}

		for (size_t c = 0; c < max; c++)
		{
			printf("%s\n", lines[c].c_str());
		}
		if (TC - 1 != 0){
			printf("\n");
		}
		cin.getline(line, MAX_LINE);
		cin.getline(line, MAX_LINE);
	}
	return 0;
}

