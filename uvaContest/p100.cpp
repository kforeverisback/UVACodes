#include <iostream>
#include <stdio.h>
#include <unordered_map>
using namespace std;

typedef unsigned long long nunit;

unsigned int pOf2[]{1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536};
std::unordered_map<nunit, nunit> prevResults;
int checkIndexOfTwo(const nunit& num)
{
	for (int i = 1; i <= 4; i)
	{
		if (num == pOf2[i++] || num == pOf2[i++] || num == pOf2[i++] || num == pOf2[i++])
		{
			return i - 1;
		}
	}
	return 0;
}

nunit algo(nunit& num)
{
	nunit printCount = 1;
	if (num == 1)
		return printCount;

	auto search = prevResults.find(num);
	if (search != prevResults.end())
	{
		//cout << "Found from memoization= " << search->first << ":"<<search->second << endl;
		return search->second;
	}
	if (num % 2 == 0)
	{
		int two = checkIndexOfTwo(num);
		if (two != 0)
		{
			return printCount + two;
		}
		num = num / 2;
	}
	else
	{
		num = num * 3 + 1;
	}
	return printCount + algo(num);
}

int main_p100(int argc, char** argv)
{
	nunit number1, number2;
	//freopen("input.txt", "r", stdin);
	while (scanf("%lld %lld\n", &number1, &number2) == 2)
	{
		nunit num1, num2;
		if (number1 < number2)
		{
			num1 = number1;
			num2 = number2;
		}
		else
		{
			num2 = number1;
			num1 = number2;
		}
		nunit pcMax = 0;
		for (nunit n = num1; n <= num2; n++)
		{
			nunit num = n;
			nunit printCount = algo(num);
			pcMax < printCount ? pcMax = printCount : 0;
			prevResults.insert(std::make_pair(n, printCount));
			//cout << "Current Print Count:: " << printCount << endl;
		}
		cout << number1 << " " << number2 << " " << pcMax << endl;
	}
	return 0;
}
