#include <cstdio>
#include <cstdlib>
#include <iostream>
using namespace std;
#ifndef ONLINE_JUDGE
static int number = 0, converted = 0;
#else
static int number asm("number") = 0, converted asm("converted")= 0;
#endif
int main(int argc, char** argv)
{
#ifndef ONLINE_JUDGE
	freopen("p594.txt", "r", stdin);
#endif
	while(scanf("%d\n", &number) >0)
	{
#if defined(__GNUC__)
		asm volatile("mov number, %eax	\n\t"
			"bswap %eax	\n\t"
			"mov %eax, converted	\n\t");
#else
		_asm{
				mov eax, number
				bswap eax
				mov converted, eax
		}
#endif
		printf("%d converts to %d\n", number, converted);
	}
}
