//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef long long           ll;
typedef unsigned int        ui;
typedef unsigned long long  ull;
typedef unsigned short      us;
typedef unsigned char       uc;

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>
#include <map>
#include <bitset>
#include <set>
#include <fstream>
#include <stack>
#include <unordered_map>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <functional> //plus, minus, multiplies, divides, modulus, equal_to,not_equal_to, greater*,less*
#include <utility> //swap, make_pair, move,

using namespace std;
typedef std::pair<int, int> pii;
typedef std::vector<int>    vi;
typedef std::vector<pii>    vpii;

//#define PI 3.1415926535897932384626
#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
#define MEMSET_INF 127 // about 2B
#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//C
#define si(_V_) scanf("%d", &_V_)
#define sii(_V_1,_V_2)  scanf("%d %d", &_V_1, &_V_2)
#define siii(_V_1,_V_2,_V_3)    scanf("%d %d %d", &_V_1,&_V_2,&_V_3)
#define sc(_V_) scanf("%c", &_V_)
#define ss(_V_) scanf("%s", _V_)
#define rep(i,n) for(int i=0, _##i=(n); i<_##i; ++i)
#define dwn(i,n) for(int i=(n); --i>0; )
#define rep_iv(i,iv,n) for(int i=iv, _##i=(n); i<_##i; ++i)
#define dwn_lv(i,lv,n) for(int i=(n); --i>=lv; )
#define forinf  for(;;)
#define whileinf while(true)
#define SafeDelete(_V_) if(nullptr != _V_) {delete _V_; _V_ = nullptr;}

#define char2Int(c) (c-'0')
#define maX(a,b)                     ( (a) > (b) ? (a) : (b))
#define miN(a,b)                     ( (a) < (b) ? (a) : (b))
#define checkbit(n,b)                ( (n >> b) & 1)
#define getbit(x,i) (x&(1<<i))  //select the bit of position i of x
#define getlowbit(x) ((x)&((x)^((x)-1))) //get the lowest bit of x
#define setbit(number,x) ((number) |= 1 << (x))
#define clearbit(number,x) ((number) &= ~(1 << (x)))
#define togglebit(number,x) ((number) ^= 1 << (x));
#define hBit(msb,n) asm("bsrl %1,%0" : "=r"(msb) : "r"(n)) //get the highest bit of x, maybe the fastest
#define abS(x) (x<0?(-x):x) // big bug here if "-x" is not surrounded by "()"

#define str_tolower(_str_) std::transform(_str_.begin(), _str_.end(), _str_.begin(), ::toupper)

//C++
#define mp std::make_pair
#define lastEle(vec) vec[vec.size()-1]
#define pb push_back
#define sz(a) (int)(a.size())


int main(int argc, char** argv)
{
    // This is sample code for testing
    return 0;
}
