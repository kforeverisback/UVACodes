//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef long long			ll;
typedef unsigned int		ui;
typedef unsigned long long	ull;
typedef unsigned short		us;
typedef unsigned char		uc;

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>
#include <map>
#include <bitset>
#include <set>
#include <fstream>
#include <stack>
#include <array>
#include <unordered_map>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <functional> //plus, minus, multiplies, divides, modulus, equal_to,not_equal_to, greater*,less*
#include <utility> //swap, make_pair, move,

using namespace std;
typedef std::pair<int, int>	pii;
typedef std::vector<int>	vi;
typedef std::vector<pii>	vpii;

//#define PI 3.1415926535897932384626
#define INF 2000000000 // 2 billion
// If you need to recall how to use memset:
#define MEMSET_INF 127 // about 2B
#define MEMSET_HALF_INF 63 // about 1B
//memset(dist, MEMSET_INF, sizeof dist); // useful to initialize shortest path distances
//memset(dp_memo, -1, sizeof dp_memo); // useful to initialize DP memoization table
//memset(arr, 0, sizeof arr); // useful to clear array of integers

//C
#define si(_V_)	scanf("%d", &_V_)
#define sii(_V_1,_V_2)	scanf("%d %d", &_V_1, &_V_2)
#define siii(_V_1,_V_2,_V_3)	scanf("%d %d %d", &_V_1,&_V_2,&_V_3)
#define sc(_V_)	scanf("%c", &_V_)
#define ss(_V_)	scanf("%s", _V_)

const char *output = "Optimal train swapping takes %d swaps.\n";
//int cmd_fn(const void* n1, const void* n2)
//{
//	int nm1 = *(int*)n1,
//		nm2 = *(int*)n2;
//	swaps += (nm1 > nm2) * 1;
//	return nm1 > nm2;
//}

template<typename T>
int sort_bubble_ret_swaps(T *start, T* end, int elemCount/*, int(__cdecl _ptr_cmp)(const T& n1, const T& n2)*/)
{
	int swap_count = 0;
	for (int i = 0; i < elemCount; i++)
	{
		T* ptr = start;
		for (; ptr != end; ptr++)
		{
			if (*ptr > *(ptr+1))
			{
				std::swap(*ptr, *(ptr+1));
				swap_count++;
			}
		}
		end--;
	}

	return swap_count;
}

int main(int argc, char** argv)
{
	//ONLINE_JUDGE is added in the Project settings :)
#ifndef ONLINE_JUDGE
	//freopen("p299.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
#endif

	//UVA FUCKING PSYCHOPATH!
	//IF IT OUTPUTS AN EXTRA "NEW LINE"
	//IT IS A FUCKING WRONG ANSWER!
	//WTF!
	int TC;
	si(TC);

	int cars[51] = {0};
	for (; TC>0; TC--)
	{
		//vi cars;
		//cars.reserve(50);
		int L;
		si(L);
		for (int i = 1; i <= L; i++)
		{
			//int car;
			si(cars[i]);
			//cars.push_back(car);
		}

		//std::sort(cars.begin(), cars.end(), cmd_fn);
		//std::sort(&cars[1], &cars[L], cmd_fn);
		//qsort(&cars[1], L, sizeof(int), cmd_fn);
		/*sort(&cars[1], &cars[L], [](int& a, int& b)
		{
			swaps += (a>b) * 1;
			return a>b;
		});*/
		//cout << cars << endl;
		int swaps = sort_bubble_ret_swaps(&cars[1], &cars[L], L);
		printf(output, swaps);
	}
	return 0;
}
