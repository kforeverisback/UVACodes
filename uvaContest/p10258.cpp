//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef long long			ll;
typedef unsigned int		ui;
typedef unsigned long long	ull;
typedef unsigned short		us;
typedef unsigned char		uc;

//C++
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <vector>
#include <map>
#include <bitset>
#include <set>
#include <fstream>
#include <stack>
#include <queue>
#include <unordered_map>

#include <algorithm> //sort, qsort, partial_soirt, stable_sort, rotate,copy*,reverse*, move*, fill*, search*, all_of, any_of, none_of, nth_element, max*, min*, binary_search, lower_bound, upper_bound,
#include <functional> //plus, minus, multiplies, divides, modulus, equal_to,not_equal_to, greater*,less*
#include <utility> //swap, make_pair, move, 

using namespace std;
typedef std::pair<int, int>	pii;
typedef std::vector<int>	vi;
typedef std::vector<pii>	vpii;

struct problem
{
	int penal_time = 0;
	bool is_completed = false;
};
typedef struct _contestant
{
	int constestant_ID = 0;
	int solved = 0;
	int penal_time = 0;
	problem probs[10];

	void print()
	{
		printf("%d %d %d\n", constestant_ID, solved, penal_time);
	}
} Contestant;

Contestant all_contestnts[102];

int main(int argc, char** argv)
{
	//ONLINE_JUDGE is added in the Project settings :)
#ifndef ONLINE_JUDGE
	//freopen("p10258.txt", "r", stdin);
	//freopen("output.txt", "w", stdout);
#endif
	
	int TC;
	cin >> TC;
	cin.ignore(2);

	auto comparer_ptr = [](const Contestant*const e1, const Contestant*const e2)
	{
		return (e1->solved > e2->solved) ? true :
			(e1->solved < e2->solved) ? false :
			(e1->penal_time < e2->penal_time) ? true :
			(e1->penal_time > e2->penal_time) ? false : e1->constestant_ID < e2->constestant_ID;
	};

	vector<Contestant*> ptr_to_c;
	//map<int, Contestant> all_contestnts;
	for (; TC > 0; TC--)
	{
		memset(all_contestnts, 0, sizeof(all_contestnts));
		int max_id = 0;

		int C, P, T;
		char S;
		for (;;)
		{
			char line[512] = { 0 };
			cin.getline(line, 512);
			if (line[0] == '\0')
				break;
			
			sscanf(line, "%d %d %d %c\n", &C, &P, &T, &S);

			//siiic(C, P, T, S);
			//printf("%d %d %d %c", C, P, T, S);

			Contestant& c = all_contestnts[C];
			if (c.constestant_ID == 0)
			{
				ptr_to_c.push_back(&c);
			}
			c.constestant_ID = C;
			//max_id = std::max(c.constestant_ID, max_id);
			if (!c.probs[P].is_completed)
			{
				switch (S)
				{
				case 'I':
					c.probs[P].penal_time += 20;
					break;
				case 'C':
					c.solved += 1;
					c.penal_time += c.probs[P].penal_time + T;
					c.probs[P].is_completed = true;
					break;
				default:
					break;
				}
			}
		}

		sort(ptr_to_c.begin(), ptr_to_c.end(), comparer_ptr);
		for (Contestant* c : ptr_to_c)
		{
			c->print();
		}
		ptr_to_c.clear();

		//UVA FUCKING PSYCHOPATH!
		//IF IT OUTPUTS AN EXTRA "NEW LINE"
		//IT IS A FUCKING WRONG ANSWER!
		//WTF!
		if (TC != 1)
			cout << endl;
	}
	return 0;
}