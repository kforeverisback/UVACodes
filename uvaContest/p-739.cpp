
//C
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

typedef unsigned short		us;
typedef unsigned char		uc;

//C++
#include <iostream>
#include <sstream>
using namespace std;
//C
#define ss(_V_)	scanf("%s", _V_)


char letters[] = { 'B', 'P', 'F', 'V',
					'C', 'S', 'K', 'G', 'J', 'Q', 'X', 'Z',
					'D', 'T',
					'L',
					'M', 'N',
					'R' };
uc codes[] = "111122222222334556";

const char * colm_10 = "         ";
const char * colm_10to35 = "                         ";
char name[21];

void calc_code(const char *name, int len, char *codeout)
{
	codeout[0] = name[0];
	char lastcode;
	const char* ccc = codeout, *nnn = name;
	const char* ptrtochr = strchr(letters, name[0]);
	if (nullptr == ptrtochr)
	{
		lastcode = ' ';
	}
	else
	{
		lastcode = codes[ptrtochr - letters];
	}

	name++; codeout++;
	for (; '\0' != name[0] && '\0' != codeout[0]; name++)
	{
		ptrtochr = strchr(letters, name[0]);
		
		if (nullptr == ptrtochr)
		{
			lastcode = ' ';
			continue;
		}
		else
		{
			char newCode = codes[ptrtochr - letters];
			if (newCode != lastcode)
			{
				lastcode = codeout[0] = newCode;
				codeout++;
			}
		}

	}
}

int main(int argc, char** argv)
{
	freopen("p-739.txt", "r", stdin);
	//freopen("p-739-out.txt", "w", stdout);
	std::stringstream strstr;
	strstr << "         NAME                     SOUNDEX CODE" << endl;
	while (EOF != ss(name))
	{
		char code[] = { '0', '0', '0', '0', '\0' };
		int namelen = strlen(name);
		const char* ptr = colm_10to35 + namelen;
		calc_code(name, namelen, code);
		strstr << colm_10 << name << ptr << code<<endl;
	}
	//LOG(strstr.str());
	strstr << "                   END OF OUTPUT" << endl;
	cout << strstr.str();
	return 0;
}