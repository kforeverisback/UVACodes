//C++ HEADER
#include <iostream>
#include <vector>
#include <stack>
#include <map>
#include <unordered_map>
#include <queue>
#include <sstream>
#include <fstream>
#include <algorithm>
//C HEADER
#include <stdio.h>

//Benchmark
//#include "CPUBenchmark.h"
using namespace std;
typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned long ulong;

uint TOTAL;
#define MOVE 0
#define PILE 1
#define QUIT 2
#define ONTO 3
#define OVER 4

#if DEBUG || _DEBUG && (_WIN32 || WIN32)
#define COUT_LOG(_X_) cout<<"==>@L"<<__LINE__<<":"<<__FUNCTION__<<"()  " << _X_
#define IF_D(_X_) if(!_X_) {cout << "Shit Happened@" << __LINE__ << ":" << __FUNCTION__ << endl;}
#else
#define IF_D(_X_) _X_
#define COUT_LOG(_X_) 
#endif

#define SAFE_DELETE(_X_) if(nullptr != _X_) {delete _X_;_X_=nullptr;}
#define SET_NULL(_X_) _X_=nullptr

template <typename T>
class element
{
private:
	element(const element<T> &);
public:
	element<T> *_next, *_back;
	T _data;

	element() : _next(nullptr), _data(-1)
	{}

	/*element(T&& data, element<T> *next) : _next(next), _data(data)
	{}*/

	element(T&& data) : _next(nullptr), _back(nullptr), _data(data)
	{}

	element(T& data) : _next(nullptr), _back(nullptr), _data(data)
	{}

	void insertNextElement(element<T> *elem)
	{
		elem->_next = _next;
		_next = elem;

	}

	element<T>* deleteNextElement()
	{
		element<T>* nextOld = _next;
		if (nullptr!=_next)
		{
			_next = nextOld->_next;
		}
		return nextOld;
	}
public:
	static void disposeElement(element<T>* elem)
	{
		SAFE_DELETE(elem);
	}
};

typedef element<int> Element;
typedef element<int>* pElement;
Element lastLocation[25];
Element elems[25];

void connect_e1_to_e2(pElement e1, pElement e2)
{
	e1->_next = e2;
	if (nullptr != e2){
		e2->_back = e1;
	}
}


void printAll(int total)
{
	pElement next = nullptr;
	for (int i = -1; i < total; )
	{
		if (next == nullptr){
			cout << endl;
			i++;
			next = &lastLocation[i];
			cout << i <<":";
		}

		cout << next->_data << "  ";
		next = next->_next;

	}
}

int main_p101(int argc, char** argv)
{
	uint val = -1;
	COUT_LOG("Logging a data " << 10 << endl);
	freopen("101.txt", "r", stdin);
	IF_D(scanf("%d\n", &TOTAL) == 1);

	char cmd[2][5] = { { 0 }, { 0 } };
	int n1, n2;
	bool contParsing = true;

	//pElement elems = new Element[TOTAL + 1];
	/*pElement *lastLocation = new pElement[TOTAL + 1];
	memset(lastLocation, 0, sizeof(pElement) * (TOTAL + 1));*/
	for (int i = 1; i <= TOTAL; i++)
	{
		lastLocation[i]._data = elems[i]._data = i;
		lastLocation[i]._next = &elems[i];
		elems[i]._back = &lastLocation[i];

	}
	//memset(elems, 0, sizeof(elems) * 26);
	while (contParsing){
		IF_D(scanf("%s %d %s %d\n", cmd[0], &n1, cmd[1], &n2) == 1);
		if (n1 == n2)
		{
			continue;
		}

		/*element_i* newElem = new element_i(10, nullptr);
		element_i* newElem1 = new element_i(50);
		element_i* newElem2 = new element_i(34);
		element_i* newElem3 = new element_i(122);
		newElem->insertNextElement(newElem1);
		newElem->_next->insertNextElement(newElem2);
		newElem->_next->insertNextElement(newElem3);
		element_i* it = newElem;
		for (int i = 0; nullptr != it; it=it->_next, i++)
		{
			cout << "node#" << i << "=" << it->_data << endl;
		}

		for (it = newElem; nullptr != it; )
		{
			element_i* em = it->_next;
			SAFE_DELETE(it);
			it = em;
		}*/

		char cmdChar = cmd[1][1];
		pElement op1, op2, temp1, temp2;
	switchGoto:
		switch (cmdChar)
		{
		//case 'm':
		//	op1 = &elems[n1];
		//	op1->_back->_next = op1->_next;
		//	SET_NULL(op1->_next);
		//	SET_NULL(op1->_back);
		//	cmdChar = cmd[1][1];
		//	goto switchGoto;
		//	break;
		//case 'p':
		//	op1 = &elems[n1];
		//	lastLocation[n1];
		//	cmdChar = cmd[1][1];
		//	goto switchGoto;
		//	break;
		case 'q':
			contParsing = false;
			break;
		case 'n':
			op1 = &elems[n1];
			op2 = elems[n2]._next;
			if (cmd[0][0] == 'm'){
				connect_e1_to_e2(op1->_back, op1->_next);
			}
			else{
				op1->_back->_next = nullptr;
				temp1 = op2->_next;
				if (nullptr != op2){
					for (; nullptr != temp1 && nullptr != temp1->_next&& temp1->_data != op2->_data; temp1 = temp1->_next){} //last elem of op1
					if (nullptr != temp1 && temp1->_data != op2->_data)
					connect_e1_to_e2(temp1, op2->_next);
				}
			}
			if (nullptr != op2){
				if (op1->_data != op2->_data)
					connect_e1_to_e2(op2, op1);
			}
			else{
				connect_e1_to_e2(&elems[n2], op1);
			}
			printAll(TOTAL);
			break;
		case 'v':
			op1 = &elems[n1];
			op2 = &elems[n2];
			if (cmd[0][0] == 'm'){
				connect_e1_to_e2(op1->_back, op1->_next);
			}
			else{
				op1->_back->_next = nullptr;				
			}

			temp1 = op2->_next;
			if (nullptr != temp1){
				for (;nullptr != temp1->_next && temp1->_data != op1->_data; temp1 = temp1->_next){} //last elem of op2
				if (temp1->_data != op1->_data)
					connect_e1_to_e2(temp1, op1);
			}
			else{
				temp1 = op2->_back;
				for (; nullptr != temp1->_back && temp1->_data != op1->_data; temp1 = temp1->_back){} //root elem of op2
				if (temp1->_data != op1->_data)
					connect_e1_to_e2(op2, op1);
			}
			printAll(TOTAL);
			break;
		}
	}

	return 0;
}
