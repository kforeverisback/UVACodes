IDIR:=./
ifeq ($(OS),Windows_NT)
	CC:=g++
	CFLAGS:=-I$(IDIR) -std=c++17 -g3 -ggdb
	MKDIR:= mkdir
else
	UNAME_S := $(shell uname -s)
	MKDIR:= mkdir -p
	ifeq ($(UNAME_S),Linux)
			CC:=g++
			CFLAGS:=-I$(IDIR) -std=c++17 -g3 -ggdb
			LDFLAGS:=-lm
	endif
	ifeq ($(UNAME_S),Darwin)
			CC:=clang++
			CFLAGS:=-I$(IDIR) -std=c++17 --stdlib=libc++ --debug
			LDFLAGS:=
	endif
endif
#DEPS:=-MMD -MT $(TARGET) -MF $(@).d
THIS_MAKEFILE_DIR:=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

FILENAME:=$(notdir ${TARGETFILE}) # Gets the filenameonly
TARGET:=$(basename ${FILENAME})
TARGETDIR:=$(dir ${TARGETFILE})
# $(warning Target File: ${TARGETFILE})
# $(warning Only File  : ${FILENAME})
# $(warning Target Name:${TARGET})
# $(warning Target Dir :${TARGETDIR})
OBJDIR:=${TARGETDIR}/obj
OUTDIR:=${TARGETDIR}/out

#USAGE: make TARGET=cpp_file_prefix
# Example: for cpp file hola.cpp do, make TARGET=hola

#_DEPS = hellomake.h
#DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
_OBJ:= ${TARGET}.o
OBJ:= $(patsubst %,${OBJDIR}/%,${_OBJ})

all: dir_create ${TARGET} simple_compile_db

${OBJDIR}/%.o: ${TARGETDIR}/${TARGET}.cpp #$(DEPS)
	${CC} -c -o $@ $< ${CFLAGS}

${TARGET}: ${OBJ}
	${CC} -o ${OUTDIR}/$@ $^ ${LDFLAGS}

.PHONY: clean dir_create simple_compile_db

dir_create:
	${MKDIR} ${OBJDIR} ${OUTDIR}

simple_compile_db:
	${THIS_MAKEFILE_DIR}/.vscode/cdb_gen.bash ${FILENAME} ${TARGETDIR} "${CC} -c -o ${OBJ} ${TARGETDIR}/${TARGET}.cpp ${CFLAGS}" ${THIS_MAKEFILE_DIR}/compile_commands.json APPEND

clean:
	rm -rf ${OBJDIR}/* ${OUTDIR}/*
	rm -rf ${THIS_MAKEFILE_DIR}/compile_commands.json